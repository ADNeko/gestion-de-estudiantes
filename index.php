<?php
session_start();
if(isset($_SESSION['id']))
  header("Location:vista/".$_SESSION['tipo_usuario']);
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Demo php for mysql</title>
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<style type="text/css">
  #panel{
    margin-left:35%;
    margin-top:15%;
    background-color:#65A4F7;
    padding: 2%;
    width:30%;
    height: 20%;
    border-radius: 5px;
  }
</style>

<body style="background-color:#1C68CB;">
  <div id="panel">
    <form method="post" role="form" action="funciones/v_verifica.php" class="login">
      <div class="form-group">
        <label for="login">ID:</label>
        <input type="text" class="form-control" name="login" id="login" placeholder="id" style="width:330px;">
      </div>

      <div class="form-group">
        <label for="pawd">Contraseña:</label>
        <input type="password" class="form-control" name="password" id="password" placeholder="pass" style="width:330px;">
      </div>

      <p class="login-submit">
        <button type="submit" class="btn btn-success" style="margin-left:265px;">Entrar</button>
      </p>
    </form>
  </div>

</body>
</html>
