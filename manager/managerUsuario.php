<?php
require_once("../repository/repositoryUsuario.php");


/**
 * Created by PhpStorm.
 * User: beto
 * Date: 08/07/16
 * Time: 07:26 PM
 */
class managerUsuario
{
    private $repositorio;
    function __construct() {
        $this->repositorio=new repositoryUsuario();
    }
    public function newUsuario(){
        return new usuario();
    }
    public function addUsuario(usuario $usuario){
        return $this->repositorio->agregarUsuario($usuario);
    }
    public function getId(usuario $usuario){
        return $this->repositorio->getId($usuario);
    }
    public function updateUsuario(usuario $usuario,$id){
        if($id!=$this->repositorio->getId($usuario)){
            return 0;
        }
          return $this->repositorio->actualizarUsuario($usuario,$id);
    }
    public function eliminarUsuario(usuario $usuario,$id){
        if($id!=$this->getId($usuario)){
            return 0;
        }
        return $this->repositorio->eliminarUsuario($id);
    }
    public function getUsuarioForDocumento($documento){
        return $this->repositorio->getUsuario($documento);
    }
    public function findAll(){
        return $this->repositorio->getAll();
    }
}
//CREAMOS UN OBJETO MANAGER USUARIO
//$managerUsuario=new managerUsuario();
////CON ESTA FUNCION CREAMOS UN NUEVO USUARIO
//$usuario=$managerUsuario->newUsuario();
//$usuario->setUserName("luis7");
//$usuario->setPassword("omg3nmhh");
//$usuario->setNombres("luis");
//$usuario->setApellidoPaterno("murillo");
//$usuario->setApellidoMaterno("vizcardo");
//$usuario->setTipoDocumento("DNI");
//$usuario->setDocumento("29643759");
//$usuario->setFechaNacimiento("1995-08-25");
//$usuario->setEmail("luis4@unsa.edu.pe");
//$usuario->setRol("administrador");
////CON ESTA FUNCION LA INSERTAMOS A LA BASE DE DATOS
//echo $managerUsuario->addUsuario($usuario);
/*//CON ESTA FUNCION ENCONTRAMOS EL ID DEL USUARIO
//$id=$managerUsuario->getId($usuario);
//$usuario->setUserName("malvado");
//$usuario->setDocumento("76696881");
//CON ESTA guardamos todos los cambios del objeto usuario EN LA BASE DE DATOS
//echo $managerUsuario->updateUsuario($usuario,$id);
//CON ESTA FUNCIONE ELIMINAMOS EL OBJETO USUARIO DE LA BASE DE DATOS
//echo $managerUsuario->eliminarUsuario($usuario);*/
//CON ESTA FUNCION ENCONTRAMOS EL OBJETO USURIO A TRAVES DE SU DNI
//echo $managerUsuario->getUsuarioForDocumento("29643751")->getNombreCompletoApellidosPrimero();
//CON ESTA FUNCION TRAEMOS TODOS LOS OBJETOS USUARIOS
///** @var usuario $user */
//foreach($managerUsuario->findAll()as $user){
//    echo $user->getNombreCompletoApellidosPrimero();
//}/*