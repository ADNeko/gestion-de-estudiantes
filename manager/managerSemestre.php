<?php

require_once("../repository/repositorySemestre.php");

/**
 * Created by PhpStorm.
 * User: beto
 * Date: 08/07/16
 * Time: 07:26 PM
 */
class managerSemestre
{
    private $repositorio;
    function __construct() {
        $this->repositorio=new repositorySemestre();
    }
    public function newSemestre(){
        return new semestre();
    }
    public function addSemestre(semestre $Semestre){
        return $this->repositorio->agregarSemestre($Semestre);
    }

    public function updateSemestre(semestre $Semestre){

          return $this->repositorio->actualizarSemestre($Semestre);
    }
    public function eliminarSemestre(semestre $Semestre){
        $id=$Semestre->getId();
        return $this->repositorio->eliminarSemestre($id);
    }
    public function getSemestre($id){
        return $this->repositorio->getSemestre($id);
    }
  
    public function findAll(carrera $carrera){
        return $this->repositorio->getAll($carrera);
    }
}

