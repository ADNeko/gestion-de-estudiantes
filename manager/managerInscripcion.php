<?php

require_once("../repository/repositoryInscripcion.php");
//require_once ("../manager/managerEstudiante.php");
//require_once ("../manager/managerCarrera.php");
/**
 * Created by PhpStorm.
 * User: beto
 * Date: 08/07/16
 * Time: 07:26 PM
 */
class managerInscripcion
{
    private $repositorio;
    function __construct() {
        $this->repositorio=new repositoryInscripcion();
    }
    public function newInscripcion(){
        return new inscripcion();
    }
    public function addInscripcion(inscripcion $inscripcion){
        return $this->repositorio->agregarInscripcion($inscripcion);
    }
    public function updateInscripcion(inscripcion $inscripcion){
        return $this->repositorio->actualizarInscripcion($inscripcion);
    }
//    public function eliminarInscripcion(inscripcion $inscripcion){
//        return $this->repositorio->eliminarInscripcion($id);
//    }
    public function getInscripcion(carrera $carrera,estudiante $estudiante){
        return $this->repositorio->getInscripcion($estudiante,$carrera);
    }
    public function getInscripcion2($id){
        return $this->repositorio->getInscripcionId($id);
    }

    public function getInscripcionEstudiante(estudiante $estudiante){
        return $this->repositorio->getInscripcionEstudiante($estudiante);
    }
//    public function findAll(){
//        return $this->repositorio->getAll();
//    }
}
//CREAMOS UN MANAGER CARRERA
//$manager=new managerInscripcion();
//$managaerE=new managerEstudiante();
//$managaerC=new managerCarrera();
////CREAMOS UNA NUEVA CARRERA
//$inscripcion=$manager->newInscripcion();
//$inscripcion->setEstado('HABILITADO');
//$inscripcion->setEstudiante($managaerE->getEstudiante("20133430"));
//$inscripcion->setCarrera($managaerC->getCarrera("CS"));

//LO INSERTAMOS EN LA BASE DE DATOS
//echo $manager->addInscripcion($inscripcion);
//echo "<BR>";
//BUSCAMOS EL OBJETO A TRAVES DE SU CODIGO
//$inscripcion=$manager->getInscripcion2(3);
//echo  $inscripcion->getEstudiante()->getNombreCompletoApellidosPrimero().'-'.utf8_decode($inscripcion->getCarrera()->getNombre());
//echo "<BR>";
//ACTUALIZAMOS SUS DATOS
//$inscripcion->setCarrera($managaerC->getCarrera("CS"));
//echo $manager->updateInscripcion($inscripcion);
//echo "<BR>";
//BUSCAMOS EL OBJETO A TRAVES DE SU CODIGO
//$inscripcion=$manager->getInscripcion2(3);
//echo  $inscripcion->getEstudiante()->getNombreCompletoApellidosPrimero().'-'.utf8_decode($inscripcion->getCarrera()->getNombre());
//echo "<BR>";
//ELIMINAMOS EL OBJETO
//echo $manager->eliminarCarrera($ca
//+rrera,$manager->getId($carrera));
