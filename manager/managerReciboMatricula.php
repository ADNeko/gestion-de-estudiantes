<?php
require_once("../repository/repositoyReciboMatricula.php");
//require_once("../manager/managerCaja.php");
//require_once("../manager/managerInscripcion.php");
//require_once("../manager/managerSemestre.php");
/**
 * Created by PhpStorm.
 * User: beto
 * Date: 08/07/16
 * Time: 07:26 PM
 */
class managerReciboMatricula
{
    private $repositorio;
    function __construct() {
        $this->repositorio=new repositoyReciboMatricula();
    }
    public function newRecibo(){
        return new ReciboMatricula();
    }
    public function addRecibo(ReciboMatricula $recibo,caja $caja){
        $manager=new managerCaja();
        $montoAgregar=bcadd('0.00',$recibo->getMonto(),2);
        $montoTotal=bcadd('0.00',$caja->getTotal(),2);
        $total=bcadd($montoTotal,$montoAgregar,2);
        $caja->setTotal($total);
        $status=$manager->updateCaja($caja);
        if($status){
            return $this->repositorio->addReciboMatricula($recibo);
        }
        else{
            return 0;
        }

    }

    public function getRecibo($id){
        return $this->repositorio->getReciboMatricula($id);
    }
    public function getReciboForInscripcionSemestre(inscripcion $inscripcion,semestre $semestre){
        return $this->repositorio->getReciboMatriculaForSemestreAndInscripcion($inscripcion,$semestre);
    }
}
/*$manager= new managerReciboMatricula();
$manager2=new managerCaja();
$manager3=new managerInscripcion();
$manager4=new managerSemestre();
$recibo=$manager->newRecibo();
$caja=$manager2->getCaja(1);
$recibo->setInscripcion($manager3->getInscripcion2(4));
$recibo->setSemestre($manager4->getSemestre(1));
$recibo->setCaja($caja);
$recibo->setMonto('50.25');
echo $manager->addRecibo($recibo,$caja);*/
