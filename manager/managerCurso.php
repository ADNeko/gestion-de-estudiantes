<?php
require_once("../repository/repositoryCurso.php");


/**
 * Created by PhpStorm.
 * User: beto
 * Date: 08/07/16
 * Time: 07:26 PM
 */
class managerCurso
{
    private $repositorio;
    function __construct() {
        $this->repositorio=new repositoryCurso();
    }
    public function newCurso(){
        return new curso();
    }
    public function addCurso(curso $curso){
        return $this->repositorio->agregarCurso($curso);
    }
    public function getId(curso $curso){
        return $this->repositorio->getId($curso);
    }

    public function updateCurso(curso $curso){
          return $this->repositorio->actualizarCurso($curso);
    }
    public function eliminarCurso(curso $curso){
        $id=$curso->getId();
        return $this->repositorio->eliminarCurso($id);
    }
    public function getCurso($codigo){
        return $this->repositorio->getCurso($codigo);
    }
  
    public function findAll(carrera $carrera){
        return $this->repositorio->getAll($carrera);
    }
    public function findAllSemestre(semestre $semestre){
        return $this->repositorio->getAllSemestre($semestre);
    }
}
