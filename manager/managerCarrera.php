<?php
require_once("../repository/repositoryCarrera.php");
//require_once ("../manager/managerCurso.php");
/**
 * Created by PhpStorm.
 * User: beto
 * Date: 08/07/16
 * Time: 07:26 PM
 */
class managerCarrera
{
    private $repositorio;
    function __construct() {
        $this->repositorio=new repositoryCarrera();
    }
    public function newCarrera(){
        return new carrera();
    }
    public function addCarrera(carrera $carrera){
        return $this->repositorio->agregarCarrera($carrera);
    }
    public function getId(carrera $carrera){
        return $this->repositorio->getId($carrera);
    }
    public function updateCarrera(carrera $carrera,$id){
        if($id!=$this->getId($carrera)){
            return 0;
        }
          return $this->repositorio->actualizarCarrera($carrera,$id);
    }
    public function getCarreraId($id){
        return $this->repositorio->getCarreraId($id);
    }
    public function eliminarCarrera(carrera $carrera,$id){
        if($id!=$this->getId($carrera)){
            return 0;
        }
        return $this->repositorio->eliminarCarrera($id);
    }
    public function getCarrera($codigo){
        return $this->repositorio->getCarrera($codigo);
    }
  
    public function findAll(){
        return $this->repositorio->getAll();
    }
}

//CREAMOS UN MANAGER CARRERA
//$manager=new managerCarrera();
//$magaercurso = new managerCurso();
//$manager3=new managerSemestre();
//CREAMOS UNA NUEVA CARRERA
//$carrera=$manager->newCarrera();
//$carrera->setCodigo("ADM");
//$carrera->setNombre("ADMINISTRACIÓN");
//LO INSERTAMOS EN LA BASE DE DATOS
//echo $manager->addCarrera($carrera);
//echo "<BR>";
//ACTUALIZAMOS SUS DATOS
//$carrera->setNombre(utf8_decode("ADMINISTRACIÓN DE EMPRESAS"));
//echo $manager->updateCarrera($carrera,$manager->getId($carrera));
//echo "<BR>";
//BUSCAMOS EL OBJETO A TRAVES DE SU CODIGO
/** @var carrera $carrera */
//$carrera2=$manager->getCarrera("CS");
//echo $carrera2->getNombre();
//echo "<BR>";
//ELIMINAMOS EL OBJETO
//11echo $manager->eliminarCarrera($carrera,$manager->getId($carrera));
//MOSTRAMOS TODOS LOS OBJETOS CARRERA
/** @var semestre $semestre */
/*foreach($carrera2->getSemestres() as $semestre){
    echo $semestre->getNombre();
    echo "<BR>";
    /** @var curso $curso */
    /*foreach($magaercurso->findAllSemestre($semestre)as $curso){
        echo $curso->getNombre();
        echo "<br>";
    }*/

//}