<?php

require_once("../repository/repositoryEstudiante.php");

/**
 * Created by PhpStorm.
 * User: beto
 * Date: 08/07/16
 * Time: 07:26 PM
 */
class managerEstudiante
{
    private $repositorio;
    function __construct() {
        $this->repositorio=new repositoryEstudiante();
    }
    public function newEstudiante(){
        return new estudiante();
    }
    public function addEstudiante(estudiante $estudiante){
        return $this->repositorio->agregarEstudiante($estudiante);
    }
    public function getId(estudiante $estudiante){
        return $this->repositorio->getId($estudiante);
    }
    public function updateEstudiante(estudiante $estudiante,$id){
        if($id!=$this->repositorio->getId($estudiante)){
            return 0;
        }
          return $this->repositorio->actualizarEstudiante($estudiante,$id);
    }
    public function eliminarEstudiante(estudiante $estudiante,$id){
        if($id!=$this->getId($estudiante)){
            return 0;
        }
        return $this->repositorio->eliminarEstudiante($id);
    }
    public function getEstudiante($valor){
        $respuesta=$this->repositorio->getEstudianteDocumento($valor);
        if($respuesta){
            return $respuesta;
        }
        else{
            return $this->repositorio->getEstudianteCodigo($valor);
        }
    }
    public function findAll(){
        return $this->repositorio->getAll();
    }
}
////CREAMOS UN MANAGER ESTUDIANTE
//$manager=new managerEstudiante();
////UN NUEVO ESTUDIANTE
//$estudiante= $manager->newEstudiante();
//$estudiante->setCodigo("20133430");
//$estudiante->setNombres("ALBERTO");
//$estudiante->setApellidoPaterno("VISA");
//$estudiante->setApellidoMaterno("Flores");
//$estudiante->setEmail("alberto@escomape.com");
//$estudiante->setTipoDocumento("DNI");
//$estudiante->setDocumento("76696881");
//$estudiante->setCelular("930353981");
//$estudiante->setFechaNacimiento("1995-08-25");

//INSERTAMOS EL ESTUDIANTE EN LA BASE DE DATOS

//echo $manager->addEstudiante($estudiante);
//EDITAMOS DATOS EN EL OBJETO ESTUDIANTE
//$estudiante->setEmail("albertovisaf@gmail.com");
//echo $manager->updateEstudiante($estudiante,$manager->getId($estudiante));
//OBTENEMOS EL ESTUDIANTE A TRAVES DE SU CODIGO O DNI
//$estudiante=$manager->getEstudiante("20133430");
//echo $estudiante->getNombreCompletoApellidosPrimero();
//ELIMINAMOS EL OBJETO ESTUDIANTE
//echo $manager->eliminarEstudiante($estudiante,$manager->getId($estudiante));
//TODOS LOS OBJETOS ESTUDIANTES
/** @var estudiante $estudiante */
//foreach($manager->findAll()as $estudiante){
//    echo $estudiante->getNombreCompletoApellidosPrimero();
//    echo "<br>";
//}