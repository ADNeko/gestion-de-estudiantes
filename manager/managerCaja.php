<?php

require_once("../repository/repositoryCaja.php");
//require_once("../manager/managerUsuario.php");

/**
 * Created by PhpStorm.
 * User: beto
 * Date: 08/07/16
 * Time: 07:26 PM
 */
class managerCaja
{
    private $repositorio;
    function __construct() {
        $this->repositorio=new repositoryCaja();
    }
    public function newCaja(){
        return new caja();
    }
    public function addCaja(caja $caja){
        if($this->repositorio->getVerificarCajaForUsuario($caja->getUsuario())){
            return $this->repositorio->agregarCaja($caja);
        }
        else{
            return -1;
        }
    }
    public function verificarCajas(usuario $usuario){
        if($this->repositorio->getVerificarCajaForUsuario($usuario)){
            return -1;
        }
        else{
            return 1;
        }
    }
    public function updateCaja(caja $caja){
        if(is_null($caja->getId())){
            return 0;
        }
          return $this->repositorio->actualizarCaja($caja);
    }

    public function getCaja($id){
        return $this->repositorio->getCaja($id);
    }
    public function getCajasForUsuario(usuario $usuario){
         return $this->repositorio->getCajaForUsuario($usuario);
    }
}
/*$manager=new managerCaja();
$manager2=new managerUsuario();
$caja=$manager->newCaja();
$caja->setUsuario($manager2->getUsuarioForDocumento(29643751));
$caja->setTotal("0.00");
$caja->setEstado("ABIERTO");
echo $manager->addCaja($caja);*/