<?php
require_once("../manager/managerReciboMatricula.php");
require_once("../repository/repositoyMatricula.php");
//require_once("../manager/managerCaja.php");
require_once("../manager/managerInscripcion.php");
require_once("../manager/managerSemestre.php");
/**
 * Created by PhpStorm.
 * User: beto
 * Date: 08/07/16
 * Time: 07:26 PM
 */
class managerMatricula
{
    private $repositorio;
    function __construct() {
        $this->repositorio=new repositoryMatricula();
    }
    public function newMatricula(){
        return new matricula();
    }
    public function addMatricula(matricula $matricula){
        $manager=new managerReciboMatricula();
        $recibo=$manager->getReciboForInscripcionSemestre($matricula->getInscripcion(),$matricula->getSemestre());

        if($recibo){
            return $this->repositorio->addmatriculaMatricula($matricula);
        }
        else{
            return 0;
        }

    }
    public function getMatricula(inscripcion $inscripcion,semestre $semestre){
        return $this->repositorio->getMatriculaForSemestreAndInscripcion($inscripcion,$semestre);
    }
}
/*$manager= new managerReciboMatricula();
$manager2=new managerCaja();
$manager3=new managerInscripcion();
$manager4=new managerSemestre();
$recibo=$manager->newRecibo();
$caja=$manager2->getCaja(1);
$recibo->setInscripcion($manager3->getInscripcion2(4));
$recibo->setSemestre($manager4->getSemestre(1));
$recibo->setCaja($caja);
$recibo->setMonto('50.25');
echo $manager->addRecibo($recibo,$caja);*/
