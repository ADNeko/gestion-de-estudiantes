<?php
require_once("semestre.php");
/**
 * Created by PhpStorm.
 * User: Genesis
 * Date: 10/07/2016
 * Time: 08:23 PM
 */
class carrera
{
    private $id;
    private $codigo;
    private $nombre;
    private $semestres;
    function __construct() {
        $this->id=null;
        $this->codigo=null;
        $this->nombre=null;
        $this->semestres=null;
    }
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    public function getId(){return $this->id;}
    public function setCodigo($codigo){
        $this->codigo=$codigo;
        return $this;
    }
    public function getCodigo(){
        return $this->codigo;
    }
    public function setNombre($nombre){

        $this->nombre=$nombre;
        return $this;
    }
    public function getNombre(){
        return $this->nombre;
    }
    public function setSemestre(semestre $semestre){
        $this->semestres[]=$semestre;
    }
    public function getSemestres(){
        return $this->semestres;
    }
}