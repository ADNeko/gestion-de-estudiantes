<?php
require_once("carrera.php");

/**
 * Created by PhpStorm.
 * User: Genesis
 * Date: 10/07/2016
 * Time: 08:23 PM
 */
class semestre
{
    private $id;
    private $nombre;
    /** @var  carrera */
    private $carrera;
    private $cursos;
    function __construct()
    {
        $this->id = null;
        $this->nombre = null;
        $this->carrera=null;
        $this->cursos=null;
    }
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    public function getId(){return $this->id;}

    public function setNombre($nombre){
        $this->nombre=$nombre;
        return $this;
    }
    public function getNombre(){
        return $this->nombre;
    }
    public function setCarrera(carrera $carrera){
        $this->carrera=$carrera;
        return $this;
    }
    public function getCarrera(){
        return $this->carrera;
    }

}