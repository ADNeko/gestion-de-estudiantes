<?php
/**
 * Created by PhpStorm.
 * User: Genesis
 * Date: 10/07/2016
 * Time: 08:23 PM
 */
class curso
{
    private $codigo;
    private $nombre;
    private $id;
    /** @var carrera $carrera */
    private $carrera;
    private $creditos;
    /** @var semestre $semestre */
    private $semestre;
    function __construct() {
        $this->id=null;
        $this->codigo=null;
        $this->nombre=null;
        $this->carrera=null;
        $this->creditos=null;
        $this->semestre=null;
    }
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    public function getId(){return $this->id;}
    public function setCodigo($codigo){
        $this->codigo=$codigo;
        return $this;
    }
    public function getCodigo(){
        return $this->codigo;
    }
    public function setNombre($nombre){
        $this->nombre=$nombre;
        return $this;
    }
    public function getNombre(){
        return $this->nombre;
    }
    public function setCreditos($creditos){
        $this->creditos=$creditos;
        return $this;
    }
    public function getCreditos(){
        return $this->creditos;
    }
    public function setCarrera(carrera $carrera){
        $this->carrera=$carrera;
        return $this;
    }
    public function getCarrera(){
        return $this->carrera;
    }

    public function setSemestre(semestre $semestre){
        $this->semestre=$semestre;
        return $this;
    }
    public function getSemestre(){
        return $this->semestre;
    }
}