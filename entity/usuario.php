<?php
/**
 * Created by PhpStorm.
 * User: beto
 * Date: 08/07/16
 * Time: 07:05 PM
 */
class usuario
{
    private $id;
    private $user_name;
    private $password;
    private $nombres;
    private $ap;
    private $am;
    private $email;
    private $fechaNacimiento;
    private $tipo_documento;
    private $documento;
    private $rol;

    function __construct() {
        $this->id=null;
        $this->user_name=null;
        $this->password=null;
        $this->nombres=null;
        $this->ap=null;
        $this->am=null;
        $this->email=null;
        $this->fechaNacimiento=null;
        $this->tipo_documento=null;
        $this->documento=null;
        $this->rol=null;
    }

    //region Métodos

     public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getNombreCompleto()
    {
        return $this->getNombres() . " " . $this->getApellidoPaterno() . " " . $this->getApellidoMaterno();
    }


    public function getNombreCompletoApellidosPrimero()
    {
        return $this->getApellidoPaterno() . ' ' . $this->getApellidoMaterno() . ', ' . $this->getNombres();
    }
    //endregion

    //region Getters and setters


    public function setApellidoPaterno($apellidoPaterno)
    {
        $this->ap = $apellidoPaterno;

        return $this;
    }

    public function getApellidoPaterno()
    {
        return $this->ap;
    }

    public function setApellidoMaterno($apellidoMaterno)
    {
        $this->am = $apellidoMaterno;

        return $this;
    }

    public function getApellidoMaterno()
    {
        return $this->am;
    }


    public function setNombres($nombres)
    {
        $this->nombres = $nombres;

        return $this;
    }


    public function getNombres()
    {
        return $this->nombres;
    }

    public function setTipoDocumento($tipoDocumento)
    {
        $this->tipo_documento = $tipoDocumento;

        return $this;
    }

    public function getTipoDocumento()
    {
        return $this->tipo_documento;
    }

    public function setDocumento($documento)
    {
        $this->documento = $documento;

        return $this;
    }

    public function getDocumento()
    {
        return $this->documento;
    }

    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }
    public function setUserName($username){
        $this->user_name=$username;
        return $this;
    }
    public function getUserName(){
        return $this->user_name;
    }
    public function setPassword($password){
        $this->password=$password;
        return $this;
    }
    public function setEmail($email){
        $this->email=$email;
        return $email;
    }
    public function getEmail(){
        return $this->email;
    }
    public function getPassword(){
        return $this->password;
    }
    public function setRol($rol)
    {
        $this->rol = $rol;

        return $this;
    }

    public function getRol()
    {
        return $this->rol;
    }

}