<?php
require("../entity/usuario.php");
/**
 * Created by PhpStorm.
 * User: Genesis
 * Date: 10/07/2016
 * Time: 08:23 PM
 */
class caja
{


    private $id;
    /** @var usuario $usuario */
    private $usuario;
    private $total;
    private $estado;
    function __construct() {
        $this->id=null;
        $this->usuario=null;
        $this->total=null;
        $this->estado=null;
    }
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    public function getId(){return $this->id;}

    public function setUsuario(usuario $usuario){
        $this->usuario=$usuario;
        return $this;
    }
    public function getUsuario(){
        return $this->usuario;
    }
    public function setTotal($total){
        $this->total=$total;
        return $this;
    }
    public function getTotal(){
        return $this->total;
    }
    public function setEstado($estado){
        $this->estado=$estado;
        return $this;
    }
    public function getEstado(){
        return $this->estado;
    }
}