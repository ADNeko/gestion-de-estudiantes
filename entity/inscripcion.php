<?php
 include_once("estudiante.php");
 include_once("carrera.php");
/**
 * Created by PhpStorm.
 * User: Genesis
 * Date: 17/07/2016
 * Time: 12:59 PM
 */
class inscripcion
{
    private $id;
    /** @var  carrera $carrera */
    private $carrera;
    /** @var  estudiante $estudiante */
    private $estudiante;
    private $estado;
    private $inscripciones;
    function __construct() {
        $this->id=null;
        $this->carrera=null;
        $this->estudiante=null;
        $this->estado=null;
        $this->inscripciones= [];
    }
    public  function setId($id){
        $this->id=$id;
        return $this;
    }
    public function getId(){
        return $this->id;
    }
    public function setCarrera(carrera $carrera){
        $this->carrera=$carrera;
        return $this;
    }
    public function getCarrera(){
        return $this->carrera;
    }
    public function setEstudiante(estudiante $estudiante){
        $this->estudiante=$estudiante;
        return $this;
    }
    public function getEstudiante(){
        return $this->estudiante;
    }
    public function  setEstado($estado){
        $this->estado=$estado;
        return $this;
    }
    public function getEstado(){
        return $this->estado;
    }
    public function getInscripciones(){
            return $this->inscripciones;
    }

}