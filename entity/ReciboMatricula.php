<?php

/**
 * Created by PhpStorm.
 * User: Genesis
 * Date: 24/07/2016
 * Time: 08:21 PM
 */
class ReciboMatricula
{
    private $id;
    /** @var  inscripcion $inscripcion */
    private $inscripcion;
    /** @var  semestre $semestre */
    private $semestre;
    /** @var  caja $caja */
    private $caja;
    private $monto;

    function __construct() {
        $this->id=null;
        $this->inscripcion=null;
        $this->semestre=null;
        $this->caja=null;
        $this->monto=null;
    }
    public function setId($id){
        $this->id=$id;
        return $this;
    }
    public function getId(){
        return $this->id;
    }
    public function setInscripcion(inscripcion $inscripcion){
        $this->inscripcion=$inscripcion;
        return $this;
    }
    public function getInscripcion(){
        return $this->inscripcion;
    }
    public function setSemestre(semestre $semestre){
        $this->semestre=$semestre;
        return $this;
    }
    public function getSemestre(){
        return $this->semestre;
    }
    public function setCaja(caja $caja){
        $this->caja=$caja;
        return $this;
    }
    public function getCaja(){
        return $this->caja;
    }
    public function setMonto($monto){
        $this->monto=$monto;
        return $this;
    }
    public function getMonto(){
        return $this->monto;
    }
}