<?php
require_once("../entity/caja.php");
require_once ("../repository/repositoryUsuario.php");
require_once("../datos/generico.class.php");
/**
 * Created by PhpStorm.
 * User: beto
 * Date: 08/07/16
 * Time: 07:53 PM
 */
class repositoryCaja
{
    private $objcon;
    private $Rusuario;

    function __construct() {
        $this->objcon=new BDgenerico();
        $this->Rusuario=new repositoryUsuario();
    }
    public function getCaja($id){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $sql="select * from caja where id='$id'";
        $result= $this->objcon->cSimple($sql);
        $resp=$this->objcon->nRegistros($result);
        if($resp>0)
        {
            $row=mysql_fetch_row($result);
            $caja=new caja();
            $caja->setId($row[0]);
            $caja->setUsuario($this->Rusuario->getUsuarioId($row[1]));
            $caja->setTotal($row[2]);
            $caja->setEstado($row[3]);

            return $caja;

        }
        else{
            return 0;
        }

    }
    public function getCajaForUsuario(usuario $usuario){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $id=$usuario->getId();
        $sql="select * from caja where cajero_id='$id' AND estado='ABIERTO'";
        $result= $this->objcon->cSimple($sql);
        $resp=$this->objcon->nRegistros($result);
        if($resp>0)
        {
            $row=mysql_fetch_row($result);
            $caja=new caja();
            $caja->setId($row[0]);
            $caja->setUsuario($this->Rusuario->getUsuarioId($row[1]));
            $caja->setTotal($row[2]);
            $caja->setEstado($row[3]);

            return $caja;

        }
        else{
            return 0;
        }

    }
    public function getVerificarCajaForUsuario(usuario $usuario){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $id=$usuario->getId();
        $sql="select * from caja where cajero_id='$id' AND estado='ABIERTO'";
        $result= $this->objcon->cSimple($sql);
        $resp=$this->objcon->nRegistros($result);
        if($resp==0){
            return true;
        }
        else{
            return false;
        }

    }
    public function agregarCaja(caja $caja){
        $this->objcon->conectar();
        $this->objcon->selectdb();


        $usuario=$caja->getUsuario()->getId();
        $total=$caja->getTotal();
        $estado=$caja->getEstado();
        $tabla="INSERT INTO caja (cajero_id,total,estado) VALUES";
        $valores="  ('$usuario','$total','$estado');";
        $sql=$tabla.$valores;

        $result=$this->objcon->cSimple($sql);
        if(!$result){
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }
    public function actualizarCaja(caja $caja){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $id=$caja->getId();
        $usuario=$caja->getUsuario()->getId();
        $total=$caja->getTotal();
        $estado=$caja->getEstado();

        $query="UPDATE caja SET cajero_id = '$usuario',total='$total',estado='$estado' WHERE id = '$id';";
        $result=$this->objcon->cSimple($query);
        if(!$result){
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }
}