<?php
require_once ("../entity/inscripcion.php");
require_once("../repository/repositoryCarrera.php");
require_once("../repository/repositoryEstudiante.php");
require_once("../datos/generico.class.php");
/**
 * Created by PhpStorm.
 * User: Genesis
 * Date: 17/07/2016
 * Time: 01:54 PM
 */
class repositoryInscripcion
{
    private $objcon;
    private $repositoryEstudiante;
    private $repositoryCarrera;

    function __construct()
    {
        $this->objcon = new BDgenerico();
        $this->repositoryCarrera = new repositoryCarrera();
        $this->repositoryEstudiante = new repositoryEstudiante();
    }

    public function getInscripcionId($id)
    {
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $sql = "select * from inscripcion where id='$id'";
        $result = $this->objcon->cSimple($sql);
        $resp = $this->objcon->nRegistros($result);
        if ($resp > 0) {
            $row = mysql_fetch_row($result);
            $inscripcion = new inscripcion();
            $inscripcion->setId($row[0]);
            $inscripcion->setCarrera($this->repositoryCarrera->getCarreraId($row[1]));
            $inscripcion->setEstudiante($this->repositoryEstudiante->getEstudianteId($row[2]));
            $inscripcion->setEstado($row[3]);

            return $inscripcion;

        } else {
            return 0;
        }

    }

    public function getInscripcionEstudiante(estudiante $estudiante)
    {
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $inscripciones=array();
        $id=$estudiante->getId();
        $sql="select * from inscripcion where estudiante_id='$id'";
        $result= $this->objcon->cSimple($sql);
        if($result){
            while ($row = $this->objcon->UnRegistro($result)) {
                $inscripcion = new inscripcion();
                $inscripcion->setId($row[0]);
                $inscripcion->setCarrera($this->repositoryCarrera->getCarreraId($row[1]));
                $inscripcion->setEstudiante($this->repositoryEstudiante->getEstudianteId($row[2]));
                $inscripcion->setEstado($row[3]);
                $inscripciones[]=$inscripcion;
            }
            return $inscripciones;
        }
        else{
            return 0;
        }
    }
    

    public function getInscripcion(estudiante $estudiante, carrera $carrera)
    {
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $id_carrera = $carrera->getId();
        $id_estudiante = $estudiante->getId();
        $sql = "select * from inscripcion where carrera_id='$id_carrera' AND estudiante_id='$id_estudiante'";
        $result = $this->objcon->cSimple($sql);
        $resp = $this->objcon->nRegistros($result);
        if ($resp > 0) {
            $row = mysql_fetch_row($result);
            $inscripcion = new inscripcion();
            $inscripcion->setId($row[0]);
            $inscripcion->setCarrera($this->repositoryCarrera->getCarreraId($row[1]));
            $inscripcion->setEstudiante($this->repositoryEstudiante->getEstudianteId($row[2]));
            $inscripcion->setEstado($row[3]);

            return $inscripcion;

        } else {
            return 0;
        }

    }
    public function agregarInscripcion(inscripcion $inscripcion){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $carrera=$inscripcion->getCarrera()->getId();
        $estudiante=$inscripcion->getEstudiante()->getId();
        $estado=$inscripcion->getEstado();
        $tabla="INSERT INTO inscripcion (carrera_id,estudiante_id,estado) VALUES";
        $valores="  ('$carrera','$estudiante','$estado');";
        $sql=$tabla.$valores;

        $result=$this->objcon->cSimple($sql);
        if(!$result){
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }
    public function actualizarInscripcion(inscripcion $inscripcion){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        if(is_null($inscripcion->getId())){
            return 0;
        }
        $id=$inscripcion->getId();
        $carrera=$inscripcion->getCarrera()->getId();
        $estudiante=$inscripcion->getEstudiante()->getId();
        $estado=$inscripcion->getEstado();

        $query="UPDATE inscripcion SET carrera_id = '$carrera',estudiante_id='$estudiante',estado='$estado' WHERE id = '$id';";
        $result=$this->objcon->cSimple($query);
        if(!$result){
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }

}