<?php
require_once("../entity/usuario.php");
require_once("../datos/generico.class.php");
/**
 * Created by PhpStorm.
 * User: beto
 * Date: 08/07/16
 * Time: 07:53 PM
 */
class repositoryUsuario
{
    private $objcon;

    function __construct() {
        $this->objcon=new BDgenerico();
    }
    public function getUsuarioId($id){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $sql="select * from usuario where id='$id'";
        $result= $this->objcon->cSimple($sql);
        $resp=$this->objcon->nRegistros($result);
        if($resp>0)
        {
            $row=mysql_fetch_row($result);
            $usuario=new usuario();
            $usuario->setId($row[0]);
            $usuario->setUserName($row[1]);
            $usuario->setPassword($row[2]);
            $usuario->setNombres($row[3]);
            $usuario->setApellidoPaterno($row[4]);
            $usuario->setApellidoMaterno($row[5]);
            $usuario->setTipoDocumento($row[6]);
            $usuario->setDocumento($row[7]);
            $usuario->setFechaNacimiento($row[8]);
            $usuario->setEmail($row[9]);
            $usuario->setRol($row[10]);
            return $usuario;

        }
        else{
            return 0;
        }

    }
    public function getUsuario($documento){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $sql="select * from usuario where documento='$documento'";
        $result= $this->objcon->cSimple($sql);
        $resp=$this->objcon->nRegistros($result);
        if($resp>0)
        {
            $row=mysql_fetch_row($result);
            $usuario=new usuario();
            $usuario->setId($row[0]);
            $usuario->setUserName($row[1]);
            $usuario->setPassword($row[2]);
            $usuario->setNombres($row[3]);
            $usuario->setApellidoPaterno($row[4]);
            $usuario->setApellidoMaterno($row[5]);
            $usuario->setTipoDocumento($row[6]);
            $usuario->setDocumento($row[7]);
            $usuario->setFechaNacimiento($row[8]);
            $usuario->setEmail($row[9]);
            $usuario->setRol($row[10]);
            return $usuario;

        }
        else{
            return 0;
        }

    }
    public function getId(usuario $usuario){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $documento=$usuario->getDocumento();
        $sql="select id from usuario where documento='$documento'";
        $result= $this->objcon->cSimple($sql);
        $resp=$this->objcon->nRegistros($result);
        if($resp>0)
        {
            $row=mysql_fetch_row($result);
            $id= $row[0];
            return $id;

        }
        else{
            return 0;
        }

    }
    public function agregarUsuario(usuario $usuario){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $username=$usuario->getUserName();
        $password=$usuario->getPassword();
        $nombres=$usuario->getNombres();
        $ap=$usuario->getApellidoPaterno();
        $am=$usuario->getApellidoMaterno();
        $tipoD=$usuario->getTipoDocumento();
        $documento=$usuario->getDocumento();
        $fecha=$usuario->getFechaNacimiento();
        $email=$usuario->getEmail();
        $rol=$usuario->getRol();

        $tabla="INSERT INTO usuario (user_name,password,nombres,apellido_paterno,apellido_materno,tipo_documento,documento,fecha_nacimiento,email,rol) VALUES";
        $valores="  ('$username','$password','$nombres', '$ap', '$am','$tipoD','$documento','$fecha','$email','$rol');";
        $sql=$tabla.$valores;

        $result=$this->objcon->cSimple($sql);
        
        if(!$result){
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }
    public function actualizarUsuario(usuario $usuario,$id){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $username=$usuario->getUserName();
        $password=$usuario->getPassword();
        $nombres=$usuario->getNombres();
        $ap=$usuario->getApellidoPaterno();
        $am=$usuario->getApellidoMaterno();
        $tipoD=$usuario->getTipoDocumento();
        $documento=$usuario->getDocumento();
        $fecha=$usuario->getFechaNacimiento();
        $email=$usuario->getEmail();
        $rol=$usuario->getRol();
        $query="UPDATE usuario SET user_name = '$username',password='$password',nombres='$nombres',apellido_paterno='$ap',apellido_materno='$am',tipo_documento='$tipoD',documento='$documento',fecha_nacimiento='$fecha',email='$email',rol='$rol' WHERE id = '$id';";
        $result=$this->objcon->cSimple($query);
        if(!$result){
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }
    public function eliminarUsuario($id){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $query="DELETE FROM usuario where id='$id';";
        $result=$this->objcon->cSimple($query);
        if(!$result){
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }
    public function getAll(){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $usuarios=array();
        $sql="select * from usuario where 1";
        $result= $this->objcon->cSimple($sql);
        if($result){
            while ($row = $this->objcon->UnRegistro($result)) {
                $usuario=new usuario();
                $usuario->setId($row[0]);
                $usuario->setUserName($row[1]);
                $usuario->setPassword($row[2]);
                $usuario->setNombres($row[3]);
                $usuario->setApellidoPaterno($row[4]);
                $usuario->setApellidoMaterno($row[5]);
                $usuario->setTipoDocumento($row[6]);
                $usuario->setDocumento($row[7]);
                $usuario->setFechaNacimiento($row[8]);
                $usuario->setEmail($row[9]);
                $usuario->setRol($row[10]);
                $usuarios[]=$usuario;
            }
            return $usuarios;
        }
        else{
            return 0;
        }
    }
}