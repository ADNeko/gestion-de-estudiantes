<?php
require_once("../entity/ReciboMatricula.php");
require_once ("../repository/repositoryInscripcion.php");
require_once ("../repository/repositorySemestre.php");
require_once ("../repository/repositoryCaja.php");
require_once("../datos/generico.class.php");
/**
 * Created by PhpStorm.
 * User: Genesis
 * Date: 24/07/2016
 * Time: 08:30 PM
 */
class repositoyReciboMatricula
{
    private $objcon;

    function __construct() {
        $this->objcon=new BDgenerico();
    }
    public function  getReciboMatricula($id){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $sql="select * from recibomatricula where id='$id'";
        $result= $this->objcon->cSimple($sql);
        $resp=$this->objcon->nRegistros($result);
        if($resp>0)
        {
            $Rcaja=new repositoryCaja();
            $RInscripcion=new repositoryInscripcion();
            $RSemestre=new repositorySemestre();
            $row=mysql_fetch_row($result);
            $recibo=new ReciboMatricula();
            $recibo->setId($row[0]);
            $recibo->setInscripcion($RInscripcion->getInscripcionId($row[1]));
            $recibo->setSemestre($RSemestre->getSemestre($row[2]));
            $recibo->setCaja($Rcaja->getCaja($row[3]));
            $recibo->setMonto($row[4]);
            return $recibo;
        }
        else{
            return 0;
        }
    }
    public function  getReciboMatriculaForSemestreAndInscripcion(inscripcion $inscripcion,semestre $semestre){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $id_s=$semestre->getId();
        $id_i=$inscripcion->getId();
        $sql="select * from recibomatricula where inscripcion_id='$id_i' and semestre_id='$id_s'";
        $result= $this->objcon->cSimple($sql);
        $resp=$this->objcon->nRegistros($result);
        if($resp>0)
        {
            $Rcaja=new repositoryCaja();
            $RInscripcion=new repositoryInscripcion();
            $RSemestre=new repositorySemestre();
            $row=mysql_fetch_row($result);
            $recibo=new ReciboMatricula();
            $recibo->setId($row[0]);
            $recibo->setInscripcion($RInscripcion->getInscripcionId($row[1]));
            $recibo->setSemestre($RSemestre->getSemestre($row[2]));
            $recibo->setCaja($Rcaja->getCaja($row[3]));
            $recibo->setMonto($row[4]);
            return $recibo;
        }
        else{
            return 0;
        }
    }
    public  function addReciboMatricula(ReciboMatricula $reciboMatricula){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $inscripcion=$reciboMatricula->getInscripcion()->getId();
        $semestre=$reciboMatricula->getSemestre()->getId();
        $caja=$reciboMatricula->getCaja()->getId();
        $monto=$reciboMatricula->getMonto();
        $tabla="INSERT INTO recibomatricula (inscripcion_id,semestre_id,caja_id,monto) VALUES";
        $valores="  ('$inscripcion','$semestre','$caja','$monto');";
        $sql=$tabla.$valores;

        $result=$this->objcon->cSimple($sql);
        if(!$result){
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }
}