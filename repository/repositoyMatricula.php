<?php
require_once("../entity/matricula.php");
require_once ("../repository/repositoryInscripcion.php");
require_once ("../repository/repositorySemestre.php");
require_once("../datos/generico.class.php");
/**
 * Created by PhpStorm.
 * User: Genesis
 * Date: 24/07/2016
 * Time: 08:30 PM
 */
class repositoryMatricula
{
    private $objcon;

    function __construct() {
        $this->objcon=new BDgenerico();
    }
    public function  getMatricula($id){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $sql="select * from matricula where id='$id'";
        $result= $this->objcon->cSimple($sql);
        $resp=$this->objcon->nRegistros($result);
        if($resp>0)
        {
            $RInscripcion=new repositoryInscripcion();
            $RSemestre=new repositorySemestre();
            $row=mysql_fetch_row($result);
            $matricula=new matricula();
            $matricula->setId($row[0]);
            $matricula->setInscripcion($RInscripcion->getInscripcionId($row[2]));
            $matricula->setSemestre($RSemestre->getSemestre($row[1]));
            return $matricula;
        }
        else{
            return 0;
        }
    }
    public function  getMatriculaForSemestreAndInscripcion(inscripcion $inscripcion,semestre $semestre){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $id_s=$semestre->getId();
        $id_i=$inscripcion->getId();
        $sql="select * from matricula where inscripcion_id='$id_i' and semestre_id='$id_s'";
        $result= $this->objcon->cSimple($sql);
        $resp=$this->objcon->nRegistros($result);
        if($resp>0)
        {
            $RInscripcion=new repositoryInscripcion();
            $RSemestre=new repositorySemestre();
            $row=mysql_fetch_row($result);
            $matricula=new matricula();
            $matricula->setId($row[0]);
            $matricula->setInscripcion($RInscripcion->getInscripcionId($row[2]));
            $matricula->setSemestre($RSemestre->getSemestre($row[1]));
            return $matricula;
        }
        else{
            return 0;
        }
    }
    public  function addmatriculaMatricula(matricula $matricula){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $inscripcion=$matricula->getInscripcion()->getId();
        $semestre=$matricula->getSemestre()->getId();
        $tabla="INSERT INTO matricula (inscripcion_id,semestre_id) VALUES";
        $valores="  ('$inscripcion','$semestre');";
        $sql=$tabla.$valores;

        $result=$this->objcon->cSimple($sql);
        if(!$result){
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }
}