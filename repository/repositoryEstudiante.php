<?php
require_once("../entity/estudiante.php");
require_once("../datos/generico.class.php");
/**
 * Created by PhpStorm.
 * User: beto
 * Date: 08/07/16
 * Time: 07:53 PM
 */
class repositoryEstudiante
{
    private $objcon;

    function __construct() {
        $this->objcon=new BDgenerico();
    }
    public function getEstudianteId($id){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $sql="select * from estudiante where id='$id'";
        $result= $this->objcon->cSimple($sql);
        $resp=$this->objcon->nRegistros($result);
        if($resp>0)
        {
            $row=mysql_fetch_row($result);
            $estudiante=new estudiante();
            $estudiante->setId($row[0]);
            $estudiante->setCodigo($row[1]);
            $estudiante->setNombres($row[2]);
            $estudiante->setApellidoPaterno($row[3]);
            $estudiante->setApellidoMaterno($row[4]);
            $estudiante->setEmail($row[5]);
            $estudiante->setTipoDocumento($row[6]);
            $estudiante->setDocumento($row[7]);
            $estudiante->setFechaNacimiento($row[8]);
            $estudiante->setCelular($row[9]);
            return $estudiante;

        }
        else{
            return 0;
        }

    }
    public function getEstudianteDocumento($documento){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $sql="select * from estudiante where documento='$documento'";
        $result= $this->objcon->cSimple($sql);
        $resp=$this->objcon->nRegistros($result);
        if($resp>0)
        {
            $row=mysql_fetch_row($result);
            $estudiante=new estudiante();
            $estudiante->setId($row[0]);
            $estudiante->setCodigo($row[1]);
            $estudiante->setNombres($row[2]);
            $estudiante->setApellidoPaterno($row[3]);
            $estudiante->setApellidoMaterno($row[4]);
            $estudiante->setEmail($row[5]);
            $estudiante->setTipoDocumento($row[6]);
            $estudiante->setDocumento($row[7]);
            $estudiante->setFechaNacimiento($row[8]);
            $estudiante->setCelular($row[9]);
            return $estudiante;

        }
        else{
            return 0;
        }

    }
    public function getEstudianteCodigo($codigo){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $sql="select * from estudiante where codigo='$codigo'";
        $result= $this->objcon->cSimple($sql);
        $resp=$this->objcon->nRegistros($result);
        if($resp>0)
        {
            $row=mysql_fetch_row($result);
            $estudiante=new estudiante();
            $estudiante->setId($row[0]);
            $estudiante->setCodigo($row[1]);
            $estudiante->setNombres($row[2]);
            $estudiante->setApellidoPaterno($row[3]);
            $estudiante->setApellidoMaterno($row[4]);
            $estudiante->setEmail($row[5]);
            $estudiante->setTipoDocumento($row[6]);
            $estudiante->setDocumento($row[7]);
            $estudiante->setFechaNacimiento($row[8]);
            $estudiante->setCelular($row[9]);
            return $estudiante;

        }
        else{
            return 0;
        }

    }
    public function getId(estudiante $estudiante){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $documento=$estudiante->getDocumento();
        $sql="select id from estudiante where documento='$documento'";
        $result= $this->objcon->cSimple($sql);
        $resp=$this->objcon->nRegistros($result);
        if($resp>0)
        {
            $row=mysql_fetch_row($result);
            $id= $row[0];
            return $id;

        }
        else{
            return 0;
        }

    }
    public function agregarEstudiante(estudiante $estudiante){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $codigo=$estudiante->getCodigo();
        $nombres=$estudiante->getNombres();
        $ap=$estudiante->getApellidoPaterno();
        $am=$estudiante->getApellidoMaterno();
        $tipoD=$estudiante->getTipoDocumento();
        $documento=$estudiante->getDocumento();
        $fecha=$estudiante->getFechaNacimiento();
        $email=$estudiante->getEmail();
        $celular=$estudiante->getCelular();

        $tabla="INSERT INTO estudiante (codigo,nombres,apellido_paterno,apellido_materno,tipo_documento,documento,fecha_nacimiento,email,celular) VALUES";
        $valores="  ('$codigo','$nombres', '$ap', '$am','$tipoD','$documento','$fecha','$email','$celular');";
        $sql=$tabla.$valores;

        $result=$this->objcon->cSimple($sql);
        if(!$result){

            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }
    public function actualizarEstudiante(estudiante $estudiante,$id){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $codigo=$estudiante->getCodigo();
        $nombres=$estudiante->getNombres();
        $ap=$estudiante->getApellidoPaterno();
        $am=$estudiante->getApellidoMaterno();
        $tipoD=$estudiante->getTipoDocumento();
        $documento=$estudiante->getDocumento();
        $fecha=$estudiante->getFechaNacimiento();
        $email=$estudiante->getEmail();
        $celular=$estudiante->getCelular();

        $query="UPDATE estudiante SET codigo = '$codigo',nombres='$nombres',apellido_paterno='$ap',apellido_materno='$am',tipo_documento='$tipoD',documento='$documento',fecha_nacimiento='$fecha',email='$email',celular='$celular' WHERE id = '$id';";
        $result=$this->objcon->cSimple($query);
        if(!$result){
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }
    public function eliminarEstudiante($id){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $query="DELETE FROM estudiante where id='$id';";
        $result=$this->objcon->cSimple($query);
        if(!$result){
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }
    public function getAll(){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $estudiantes=array();
        $sql="select * from estudiante where 1";
        $result= $this->objcon->cSimple($sql);
        if($result){
            while ($row = $this->objcon->UnRegistro($result)) {
                $estudiante=new estudiante();
                $estudiante->setId($row[0]);
                $estudiante->setCodigo($row[1]);
                $estudiante->setNombres($row[2]);
                $estudiante->setApellidoPaterno($row[3]);
                $estudiante->setApellidoMaterno($row[4]);
                $estudiante->setEmail($row[5]);
                $estudiante->setTipoDocumento($row[6]);
                $estudiante->setDocumento($row[7]);
                $estudiante->setFechaNacimiento($row[8]);
                $estudiante->setCelular($row[9]);
                $estudiantes[]=$estudiante;
            }
            return $estudiantes;
        }
        else{
            return 0;
        }
    }
}