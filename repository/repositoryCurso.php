<?php
require_once("../repository/repositoryCarrera.php");
require_once("../repository/repositorySemestre.php");
require_once("../entity/curso.php");
require_once("../datos/generico.class.php");
/**
 * Created by PhpStorm.
 * User: beto
 * Date: 08/07/16
 * Time: 07:53 PM
 */
class repositoryCurso
{
    private $objcon;
    private $Rcarrera;
    private $Rsemestre;
    function __construct() {
        $this->objcon=new BDgenerico();
        $this->Rcarrera=new repositoryCarrera();
        $this->Rsemestre= new repositorySemestre();
    }
    public function getCurso($codigo){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $sql="select * from curso where codigo='$codigo'";
        $result= $this->objcon->cSimple($sql);
        $resp=$this->objcon->nRegistros($result);
        if($resp>0)
        {
            $row=mysql_fetch_row($result);
            $curso=new curso();
            $curso->setId($row[0]);
            $curso->setCodigo($row[1]);
            $curso->setNombre($row[2]);
            $curso->setCreditos($row[3]);
            $curso->setCarrera($this->Rcarrera->getCarreraId($row[4]));
            $curso->setSemestre($this->Rsemestre->getSemestre($row[5]));
            return $curso;

        }
        else{
            return 0;
        }

    }
    public function getCursoId($id){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $sql="select * from curso where id='$id'";
        $result= $this->objcon->cSimple($sql);
        $resp=$this->objcon->nRegistros($result);
        if($resp>0)
        {
            $row=mysql_fetch_row($result);
            $curso=new curso();
            $curso->setId($row[0]);
            $curso->setCodigo($row[1]);
            $curso->setNombre($row[2]);
            $curso->setCreditos($row[3]);
            $curso->setCarrera($this->Rcarrera->getCarreraId($row[4]));
            $curso->setSemestre($this->Rsemestre->getSemestre($row[5]));
            return $curso;

        }
        else{
            return 0;
        }

    }
    public function getId(curso $curso){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $codigo=$curso->getCodigo();
        $sql="select id from curso where codigo='$codigo'";
        $result= $this->objcon->cSimple($sql);
        $resp=$this->objcon->nRegistros($result);
        if($resp>0)
        {
            $row=mysql_fetch_row($result);
            $id= $row[0];
            return $id;

        }
        else{
            return 0;
        }

    }
    public function agregarCurso(curso $curso){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $codigo=$curso->getCodigo();
        $nombre=$curso->getNombre();
        $creditos=$curso->getCreditos();
        $semestre=$curso->getSemestre()->getId();
        $carrera=$this->Rcarrera->getId($curso->getCarrera());
        $tabla="INSERT INTO curso (codigo,nombre,creditos,carrera_id,semestre_id) VALUES";
        $valores="  ('$codigo','$nombre','$creditos','$carrera','$semestre');";
        $sql=$tabla.$valores;

        $result=$this->objcon->cSimple($sql);
        if(!$result){
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }
    public function actualizarCurso(curso $curso){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $id=$curso->getId();
        $codigo=$curso->getCodigo();
        $nombre=$curso->getNombre();
        $creditos=$curso->getCreditos();
        $carrera=$curso->getCarrera()->getId();
        $semestre=$curso->getSemestre()->getId();

        $query="UPDATE curso SET codigo = '$codigo',nombre='$nombre',creditos='$creditos',carrera_id='$carrera',semestre_id='$semestre' WHERE id = '$id';";
        $result=$this->objcon->cSimple($query);
        if(!$result){
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }
    public function eliminarCurso($id){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $query="DELETE FROM curso where id='$id';";
        $result=$this->objcon->cSimple($query);
        if(!$result){
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }
    public function getAll(carrera $carrera){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $cursos=array();
        $id=$carrera->getId();
        $sql="select * from curso where carrera_id='$id'";
        $result= $this->objcon->cSimple($sql);
        if($result){
            while ($row = $this->objcon->UnRegistro($result)) {
                $curso=new curso();
                $curso->setId($row[0]);
                $curso->setCodigo($row[1]);
                $curso->setNombre($row[2]);
                $curso->setCreditos($row[3]);
                $curso->setCarrera($carrera);
                $curso->setSemestre($this->Rsemestre->getSemestre($row[5]));
                $cursos[]=$curso;
            }
            return $cursos;
        }
        else{
            return 0;
        }
    }
    public function getAllSemestre(semestre $semestre){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $cursos=array();
        $id=$semestre->getId();
        $sql="select * from curso where semestre_id='$id'";
        $result= $this->objcon->cSimple($sql);
        if($result){
            while ($row = $this->objcon->UnRegistro($result)) {
                $curso=new curso();
                $curso->setId($row[0]);
                $curso->setCodigo($row[1]);
                $curso->setNombre($row[2]);
                $curso->setCreditos($row[3]);
                $curso->setCarrera($this->Rcarrera->getCarreraId($row[4]));
                $curso->setSemestre($semestre);
                $cursos[]=$curso;
            }
            return $cursos;
        }
        else{
            return 0;
        }
    }
}