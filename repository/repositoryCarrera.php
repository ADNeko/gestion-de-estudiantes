<?php
require_once ("../entity/carrera.php");
require_once("../datos/generico.class.php");
/**
 * Created by PhpStorm.
 * User: beto
 * Date: 08/07/16
 * Time: 07:53 PM
 */
class repositoryCarrera
{
    private $objcon;

    function __construct()
    {
        $this->objcon = new BDgenerico();
    }

    public function getCarrera($codigo)
    {
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $sql = "select * from carrera where codigo='$codigo'";
        $result = $this->objcon->cSimple($sql);
        $resp = $this->objcon->nRegistros($result);
        if ($resp > 0) {
            $row = mysql_fetch_row($result);
            $carrera = new carrera();
            $carrera->setId($row[0]);
            $carrera->setCodigo($row[1]);
            $carrera->setNombre($row[2]);
            $carrera = $this->getSemestres($carrera);
            return $carrera;

        }
        else {
            return 0;
        }

    }

    public function getCarreraId($id)
    {
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $sql = "select * from carrera where id='$id'";
        $result = $this->objcon->cSimple($sql);
        $resp = $this->objcon->nRegistros($result);
        if ($resp > 0) {
            $row = mysql_fetch_row($result);
            $carrera = new carrera();
            $carrera->setId($row[0]);
            $carrera->setCodigo($row[1]);
            $carrera->setNombre($row[2]);
            $carrera = $this->getSemestres($carrera);
            return $carrera;

        } else {
            return 0;
        }

    }

    public function getId(carrera $carrera)
    {
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $codigo = $carrera->getCodigo();
        $sql = "select id from carrera where codigo='$codigo'";
        $result = $this->objcon->cSimple($sql);
        $resp = $this->objcon->nRegistros($result);
        if ($resp > 0) {
            $row = mysql_fetch_row($result);
            $id = $row[0];
            return $id;

        } else {
            return 0;
        }

    }

    public function agregarCarrera(carrera $carrera)
    {
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $codigo = $carrera->getCodigo();
        $nombre = $carrera->getNombre();

        $tabla = "INSERT INTO carrera (codigo,nombre) VALUES";
        $valores = "  ('$codigo','$nombre');";
        $sql = $tabla . $valores;

        $result = $this->objcon->cSimple($sql);
        if (!$result) {
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }

    public function actualizarCarrera(carrera $carrera, $id)
    {
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $codigo = $carrera->getCodigo();
        $nombre = $carrera->getNombre();


        $query = "UPDATE carrera SET codigo = '$codigo',nombre='$nombre' WHERE id = '$id';";
        $result = $this->objcon->cSimple($query);
        if (!$result) {
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }

    public function eliminarCarrera($id)
    {
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $query = "DELETE FROM carrera where id='$id';";
        $result = $this->objcon->cSimple($query);
        if (!$result) {
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }

    public function getAll()
    {
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $carreras = array();
        $sql = "select * from carrera where 1";
        $result = $this->objcon->cSimple($sql);
        if ($result) {
            while ($row = $this->objcon->UnRegistro($result)) {
                $carrera = new carrera();
                $carrera->setId($row[0]);
                $carrera->setCodigo($row[1]);
                $carrera->setNombre(utf8_decode($row[2]));
                $carrera = $this->getSemestres($carrera);
                $carreras[] = $carrera;
            }
            return $carreras;
        } else {
            return 0;
        }
    }

    public function getSemestres(carrera $carrera)
    {
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $id = $carrera->getId();
        $sql = "select * from semestre where carrera_id='$id'";
        $result = $this->objcon->cSimple($sql);
        if ($result) {
            while ($row = $this->objcon->UnRegistro($result)) {
                $semestre = new semestre();
                $semestre->setId($row[0]);
                $semestre->setNombre($row[1]);
                $semestre->setCarrera($carrera);
                $carrera->setSemestre($semestre);
            }
        }
        return $carrera;
    }
}