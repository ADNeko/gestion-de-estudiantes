<?php
require_once ("../repository/repositoryCarrera.php");
require_once("../datos/generico.class.php");
/**
 * Created by PhpStorm.
 * User: beto
 * Date: 08/07/16
 * Time: 07:53 PM
 */
class repositorySemestre
{
    private $objcon;
    private $repository;
    private $repositoryCursos;
    function __construct() {
        $this->objcon=new BDgenerico();
        $this->repository= new repositoryCarrera();

    }
    public function getSemestre($id){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $sql="select * from semestre where id='$id'";
        $result= $this->objcon->cSimple($sql);
        $resp=$this->objcon->nRegistros($result);
        if($resp>0)
        {
            $row=mysql_fetch_row($result);
            $semestre=new semestre();
            $semestre->setId($row[0]);
            $semestre->setNombre($row[1]);
            $semestre->setCarrera($this->repository->getCarreraId($row[2]));

            return $semestre;

        }
        else{
            return 0;
        }

    }
    public function agregarSemestre(semestre $semestre){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $id=$semestre->getId();
        $nombre=$semestre->getNombre();
        $carrera=$semestre->getCarrera()->getId();
        $tabla="INSERT INTO semestre (id,nombre,carrera_id) VALUES";
        $valores="  ('$id','$nombre','$carrera');";
        $sql=$tabla.$valores;

        $result=$this->objcon->cSimple($sql);
        if(!$result){
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }
    public function actualizarSemestre(semestre $semestre){
        $this->objcon->conectar();
        $this->objcon->selectdb();

        $id=$semestre->getId();
        $nombre=$semestre->getNombre();
        $carrera=$semestre->getCarrera()->getId();
        $query="UPDATE semestre SET nombre = '$nombre',carrera_id='$carrera' WHERE id = '$id';";
        $result=$this->objcon->cSimple($query);
        if(!$result){
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }
    public function eliminarSemestre($id){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $query="DELETE FROM semestre where id='$id';";
        $result=$this->objcon->cSimple($query);
        if(!$result){
            $this->objcon->desconectar();
            return 0;
        }
        $this->objcon->desconectar();
        return $result;
    }
    public function getAll(carrera $carrera){
        $this->objcon->conectar();
        $this->objcon->selectdb();
        $id=$carrera->getId();
        $semestres=array();
        $sql="select * from semestre where carrera_id='$id'";
        $result= $this->objcon->cSimple($sql);
        if($result){
            while ($row = $this->objcon->UnRegistro($result)) {
                $semestre=new semestre();
                $semestre->setId($row[0]);
                $semestre->setNombre($row[1]);
                $semestre->setCarrera($this->repository->getCarreraId($row[2]));
                $semestres[]=$semestre;
            }
            return $semestres;
        }
        else{
            return 0;
        }
    }
}