<?php
require_once("../manager/managerCarrera.php");


/**
 * Created by PhpStorm.
 * User: beto
 * Date: 11/07/16
 * Time: 03:33 PM
 */

$id=$_POST["id"];

$manager= new managerCarrera();
$carrera=$manager->getCarreraId($id);

$status   = $manager->eliminarCarrera($carrera,$id);

session_start();
$var=array();
$var=$manager->findAll();
$_SESSION['carreras']=$var;

header('Location:../vista/administrador/carreras.php?status3='.$status.'&status=1');
