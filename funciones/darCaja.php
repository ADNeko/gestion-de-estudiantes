<?php
	
	require_once("../manager/managerCaja.php");
	require_once("../manager/managerUsuario.php");
	require_once("../entity/usuario.php");
	session_start();
	/**
	 * Created by PhpStorm.
	 * User: beto
	 * Date: 11/07/16
	 * Time: 03:33 PM
	 */

	$doc=$_SESSION['documento'];

	$managerU=new managerUsuario();
	$usuario=$managerU->getUsuarioForDocumento($doc);

	$manager= new managerCaja();

	if($manager->verificarCajas($usuario)==1){
		$caja=$manager->getCajasForUsuario($usuario);
		$status=2;
		header('Location:../vista/administrador/miCaja.php?status='.$status.'&estado='.$caja->getEstado().'&total='.$caja->getTotal());
	}
	else{
		$status=-2;
		header('Location:../vista/administrador/miCaja.php?status='.$status.'&estado=0&total=0');
	}

?>