<?php

require_once("../manager/managerEstudiante.php");
require_once("../manager/managerInscripcion.php");
require_once("../manager/managerCarrera.php");

/**
 * Created by PhpStorm.
 * User: beto
 * Date: 11/07/16
 * Time: 03:33 PM
 */


function generarCodigo(){
	$cod=20160000+rand(1000,9999);
	$manager= new managerEstudiante();
	$verificar=$manager->getEstudiante($cod);
	while($verificar==1){
		$cod=20160000+rand(1000,9999);
		$verificar=$manager->getEstudiante($cod);
	}
	return $cod;
}

function is_dni($n_doc){
	if(strlen($n_doc)==8){
		return true;
	}
	else{
		return false;
	}
}


$nombres=$_POST["name"];
$codigo=generarCodigo();
$ap=$_POST["ap"];
$am=$_POST["am"];
$tipoD=$_POST["tipod"];
$documento=$_POST["documento"];
$fecha=$_POST["fechaNacimiento"];
$email=$_POST["email"];
$cel=$_POST["phone"];
$idC=$_POST["carrera"];


$manager= new managerEstudiante();
$estudiante=$manager->newEstudiante();
$estudiante->setCodigo($codigo);
$estudiante->setNombres($nombres);
$estudiante->setApellidoPaterno($ap);
$estudiante->setApellidoMaterno($am);
$estudiante->setTipoDocumento($tipoD);
$estudiante->setFechaNacimiento($fecha);
$estudiante->setDocumento($documento);
$estudiante->setCelular($cel);
$estudiante->setEmail($email);

$status=0;

if(!is_null($nombres)&&!is_null($ap)&&!is_null($documento)&&!is_null($email)&&!is_null($codigo) && is_numeric($documento) && is_numeric($cel) && is_dni($documento)){//COMPLETAS LO QUE FALTA ES PARA VALIDAR LOS DATOS QUE ENVIAN POR EL FORMULARIO
    $status=$manager->addEstudiante($estudiante);
}
else{
    $status=2;
}
if($status==1){
	$estudiante->setId($manager->getId($estudiante));

	$managerI=new managerInscripcion();
	$managerC=new managerCarrera();

	$carrera=$managerC->getCarreraId($idC);

	$inscripcion=$managerI->newInscripcion();
	$inscripcion->setCarrera($carrera);
	$inscripcion->setEstudiante($estudiante);
	$inscripcion->setEstado("habilitado");

	$status=$managerI->addInscripcion($inscripcion);
}

header('Location: ../vista/administrador/inscribirEstudiante.php?status='.$status);

?>