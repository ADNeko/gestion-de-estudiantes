<?php

	require_once("../manager/managerReciboMatricula.php");
	require_once("../manager/managerInscripcion.php");
	require_once("../manager/managerSemestre.php");
	require_once("../manager/managerCaja.php");
	require_once("../manager/managerUsuario.php");

	require_once("../entity/usuario.php");

	session_start();

	$idC=$_POST["idC"];
	$idI=$_POST["idI"];
	$idS=$_POST['semestre'];
	$monto=$_POST["monto"];

	$doc=$_SESSION['documento'];

	$managerU=new managerUsuario();
	$usuario=$managerU->getUsuarioForDocumento($doc);

	$managerC= new managerCaja();
	$caja=$managerC->getCajasForUsuario($usuario);

	$managerI=new managerInscripcion();
	$inscripcion=$managerI->getInscripcion2($idI);

	$managerS=new managerSemestre();
	$semestre=$managerS->getSemestre($idS);

	$managerR=new managerReciboMatricula();
	$recibo=$managerR->newRecibo();
	$recibo->setInscripcion($inscripcion);
	$recibo->setCaja($caja);
	$recibo->setSemestre($semestre);
	$recibo->setMonto($monto);

	$status=$managerR->addRecibo($recibo,$caja);

	header('Location:../vista/administrador/pagarMatricula.php?status=1&status2='.$status.'&idC='.$idC.'&idI='.$idI);
?>
