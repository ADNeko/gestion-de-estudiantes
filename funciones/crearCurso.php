<?php

	require_once("../manager/managerCurso.php");
	require_once("../manager/managerCarrera.php");
	require_once("../manager/managerSemestre.php");

	//CREAMOS UN MANAGER CARRERA
	$manager=new managerCurso();
	$manager2=new managerSemestre();
	$manager3=new managerCarrera();

	$idC=$_POST["idCarrera"];
	$idS=$_POST["idSemestre"];
	$codigo=$_POST["codigo"];
	$nombre=$_POST["nombre"];
	$creditos=$_POST["creditos"];

	$carrera=$manager3->getCarreraId($idC);
	$semestre=$manager2->getSemestre($idS);

	$curso=$manager->newCurso();
	$curso->setCarrera($carrera);
	$curso->setSemestre($semestre);
	$curso->setNombre($nombre);
	$curso->setCreditos($nombre);
	$curso->setCodigo($codigo);

	$status=$manager->addCurso($curso);
	
	session_start();
	$var=array();
	$var=$manager3->findAll();
	$_SESSION['carreras']=$var;
	
	header('Location:../vista/administrador/carreras.php?status4='.$status.'&status=1');
?>
