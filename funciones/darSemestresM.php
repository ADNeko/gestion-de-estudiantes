<?php

	require_once("../manager/managerCarrera.php");
	require_once("../manager/managerSemestre.php");

	//CREAMOS UN MANAGER CARRERA
	$manager=new managerCarrera();
	$manager3=new managerSemestre();

	$idC=$_GET["idC"];
	$idI=$_GET["idI"];

	$carrera=$manager->getCarreraId($idC);
	
	$semestres=array();
	$ids=array();

	foreach ($carrera->getSemestres() as $semestre) {
		$semestres[]=$semestre->getNombre();
		$ids[]=$semestre->getId();
	}

	session_start();
	$_SESSION['semestres']=$semestres;
	$_SESSION['idSemestres']=$ids;

	header('Location:../vista/administrador/matricular.php?status=1&idC='.$idC.'&idI='.$idI);
?>
