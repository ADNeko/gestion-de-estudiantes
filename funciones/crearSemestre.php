<?php

	require_once("../manager/managerCarrera.php");
	require_once("../manager/managerSemestre.php");

	//CREAMOS UN MANAGER CARRERA
	$manager=new managerCarrera();
	$manager3=new managerSemestre();

	$id=$_POST["id"];
	$nombre=$_POST["nombre"];

	$carrera=$manager->getCarreraId($id);
	
	$semestre=$manager3->newSemestre();
	$semestre->setNombre($nombre);
	$semestre->setCarrera($carrera);
	$manager3->addSemestre($semestre);

	header('Location:verCarrera.php?status=1&nombre='.$carrera->getNombre().'&id='.$id);
?>
