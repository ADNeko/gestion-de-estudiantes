<?php

	require_once("../manager/managerCarrera.php");
	require_once("../manager/managerCurso.php");
	require_once("../manager/managerSemestre.php");

	//CREAMOS UN MANAGER CARRERA
	$manager=new managerCarrera();
	$magaercurso = new managerCurso();
	$manager3=new managerSemestre();

	$id=$_GET["id"];
	$nombre=$_GET["nombre"];

	$carrera=$manager->getCarreraId($id);
	
	$semestres=array();
	$ids=array();

	foreach ($carrera->getSemestres() as $semestre) {
		$semestres[]=$semestre->getNombre();
		$ids[]=$semestre->getId();
	}

	$cursos=array();
	$cursosSemestres=array();

	/** @var semestre $semestre */
	foreach($carrera->getSemestres() as $semestre){
	    /** @var curso $curso */
	    foreach ($magaercurso->findAllSemestre($semestre) as $curso) {
	    	$cursos[]=$curso->getNombre();
	    }
	    $cursosSemestres[]=$cursos;
	    $cursos=array();
	}

	session_start();
	$_SESSION['semestres']=$semestres;
	$_SESSION['datos']=$cursosSemestres;
	$_SESSION['idSemestres']=$ids;

	header('Location:../vista/administrador/verCarrera.php?status=1&nombre='.$nombre.'&id='.$id);
?>
