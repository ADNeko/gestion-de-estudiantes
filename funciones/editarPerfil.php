<?php

require_once("../manager/managerUsuario.php");

/**
 * Created by PhpStorm.
 * User: beto
 * Date: 11/07/16
 * Time: 03:33 PM
 */

$nickname=$_POST["usuario1"];
$pass=$_POST["password1"];
$nombres=$_POST["name1"];
$ap=$_POST["ap1"];
$am=$_POST["am1"];
$tipoD=$_POST["tipod1"];
$documento=$_POST["documento1"];
$fecha=$_POST["fechaNacimiento1"];
$email=$_POST["email1"];
$rol=$_POST["rol1"];


$manager= new managerUsuario();
$usuario=$manager->newUsuario();
$usuario->setUserName($nickname);
$usuario->setPassword($pass);
$usuario->setNombres($nombres);
$usuario->setApellidoPaterno($ap);
$usuario->setApellidoMaterno($am);
$usuario->setTipoDocumento($tipoD);
$usuario->setFechaNacimiento($fecha);
$usuario->setDocumento($documento);
$usuario->setRol($rol);
$usuario->setEmail($email);


$status=0;

if(!is_null($nickname)&&!is_null($pass)&&!is_null($documento)&&!is_null($nombres)){//COMPLETAS LO QUE FALTA ES PARA VALIDAR LOS DATOS QUE ENVIAN POR EL FORMULARIO
	$status   = $manager->updateUsuario($usuario,$manager->getId($usuario));

}
else{
	$status   = 2;
}
header('Location: ../vista/administrador/miPerfil.php?status=1&status1='.$status.'&user='.$nickname.'&pass='.$pass.'&name='.$nombres.'&ap='.$ap.'&am='.$am.'&td='.$tipoD.'&doc='.$documento.'&fn='.$fecha.'&email='.$email.'&rol='.$rol);
?>