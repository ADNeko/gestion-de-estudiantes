<?php
require_once("../manager/managerUsuario.php");

function is_dni($n_doc){
	if(strlen($n_doc)==8){
		return true;
	}
	else{
		return false;
	}
}

/**
 * Created by PhpStorm.
 * User: beto
 * Date: 11/07/16
 * Time: 03:33 PM
 */

$nickname=$_POST["usuario"];
$pass=$_POST["password"];
$nombres=$_POST["name"];
$ap=$_POST["ap"];
$am=$_POST["am"];
$tipoD=$_POST["tipod"];
$documento=$_POST["documento"];
$fecha=$_POST["fechaNacimiento"];
$email=$_POST["email"];
$rol=$_POST["rol"];

$manager= new managerUsuario();
$usuario=$manager->newUsuario();
$usuario->setUserName($nickname);
$usuario->setPassword($pass);
$usuario->setNombres($nombres);
$usuario->setApellidoPaterno($ap);
$usuario->setApellidoMaterno($am);
$usuario->setTipoDocumento($tipoD);
$usuario->setFechaNacimiento($fecha);
$usuario->setDocumento($documento);
$usuario->setRol($rol);
$usuario->setEmail($email);



$status=0;

if(!is_null($nickname)&&!is_null($pass)&&!is_null($documento)&&!is_null($nombres) && is_numeric($documento) && is_dni($documento)){//COMPLETAS LO QUE FALTA ES PARA VALIDAR LOS DATOS QUE ENVIAN POR EL FORMULARIO
	$status   = $manager->addUsuario($usuario);
}
else{
	$status   = 2;
}

header('Location: ../vista/administrador/nuevoUsuario.php?status='.$status);	