<?php

require_once("../manager/managerEstudiante.php");
require_once("../manager/managerInscripcion.php");

/**
 * Created by PhpStorm.
 * User: beto
 * Date: 11/07/16
 * Time: 03:33 PM
 */


$tipoD=$_POST["tipod"];
$documento=$_POST["documento"];


$manager= new managerEstudiante();
$estudiante=$manager->getEstudiante($documento);
$status=0;

if($estudiante!=null){
	$status=1;

	$nombre=$estudiante->getNombres();
	$ap=$estudiante->getApellidoPaterno();
	$am=$estudiante->getApellidoMaterno();
	$td=$estudiante->getTipoDocumento();
	$doc=$estudiante->getDocumento();
	$fn=$estudiante->getFechaNacimiento();
	$email=$estudiante->getEmail();
	$cel=$estudiante->getCelular();
	$code=$estudiante->getCodigo();

	$managerI=new managerInscripcion();
	$inscripciones=$managerI->getInscripcionEstudiante($estudiante);
	session_start();
	$_SESSION["inscripciones"]=$inscripciones;

	header('Location: ../vista/administrador/buscarEstudiante.php?status='.$status.'&name='.$nombre.'&ap='.$ap.'&am='.$am.'&td='.$td.'&doc='.$doc.'&fn='.$fn.'&email='.$email.'&cel='.$cel.'&code='.$code);

}
else{
	header('Location: ../vista/administrador/buscarEstudiante.php?status='.$status);	
}

?>