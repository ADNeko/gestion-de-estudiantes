<?php
	
	require_once("../manager/managerCarrera.php");
	require_once("../entity/carrera.php");

	$id=$_POST["id"];
	$codigo=$_POST["codigo"];
	$nombre=$_POST["nombre"];

	$manager= new managerCarrera();
	$carrera=$manager->newCarrera();
	$carrera->setId($id);
	$carrera->setCodigo($codigo);
	$carrera->setNombre($nombre);

	$status=0;

	if(!is_null($nombre)&&!is_null($codigo)){//COMPLETAS LO QUE FALTA ES PARA VALIDAR LOS DATOS QUE ENVIAN POR EL FORMULARIO
		$status=$manager->updateCarrera($carrera,$id);
		session_start();
		$var=array();
		$var=$manager->findAll();
		$_SESSION['carreras']=$var;
	}

	header('Location:../vista/administrador/carreras.php?status1='.$status.'&status=1');

?>