<?php

require_once("../manager/managerUsuario.php");

/**
 * Created by PhpStorm.
 * User: beto
 * Date: 11/07/16
 * Time: 03:33 PM
 */


$tipoD=$_POST["tipod"];
$documento=$_POST["documento"];
$rol=$_POST["rol"];


$manager= new managerUsuario();
$usuario=$manager->getUsuarioForDocumento($documento);
$status=0;


if($usuario!=null){
	$status=1;

	$id=$usuario->getId();
	$nombre=$usuario->getNombres();
	$user=$usuario->getUserName();
	$pass=$usuario->getPassword();
	$ap=$usuario->getApellidoPaterno();
	$am=$usuario->getApellidoMaterno();
	$fn=$usuario->getFechaNacimiento();
	$email=$usuario->getEmail();

	header('Location: ../vista/administrador/buscarUsuario.php?status='.$status.'&user='.$user.'&pass='.$pass.'&name='.$nombre.'&ap='.$ap.'&am='.$am.'&td='.$tipoD.'&doc='.$documento.'&fn='.$fn.'&email='.$email.'&rol='.$rol.'&id='.$id);
}
else{
	$status=0;
	header('Location: ../vista/administrador/buscarUsuario.php?status='.$status);
}

?>