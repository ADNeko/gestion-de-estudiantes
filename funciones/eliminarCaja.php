<?php
	
	require_once("../manager/managerCaja.php");
	require_once("../manager/managerUsuario.php");
	require_once("../entity/usuario.php");
	session_start();
	/**
	 * Created by PhpStorm.
	 * User: beto
	 * Date: 11/07/16
	 * Time: 03:33 PM
	 */

	$doc=$_SESSION['documento'];

	$managerU=new managerUsuario();
	$usuario=$managerU->getUsuarioForDocumento($doc);

	$manager=new managerCaja();

	$caja=$manager->getCajasForUsuario($usuario);
	$caja->setEstado("CERRADO");
	$status=$manager->updateCaja($caja);

	header('Location:../vista/administrador/miCaja.php?status1='.$status.'&estado='.$caja->getEstado().'&total='.$caja->getTotal());

?>