<?php
	
	require_once("../manager/managerSemestre.php");
	require_once("../manager/managerCarrera.php");
	require_once("../entity/semestre.php");

	$id=$_POST["id"];
	$idC=$_POST["idC"];
	$nombre=$_POST["nombre"];

	$managerC= new managerCarrera();
	$carrera=$managerC->getCarreraId($idC);

	$manager= new managerSemestre();
	$semestre=$manager->newSemestre();
	$semestre->setId($id);
	$semestre->setNombre($nombre);
	$semestre->setCarrera($carrera);

	$status=0;

	if(!is_null($nombre)){
		$status=$manager->updateSemestre($semestre);

	}

	header('Location:../vista/administrador/carreras.php?status6='.$status.'&status=1');

?>