<?php
session_start();

if(!isset($_SESSION['id']) or $_SESSION['tipo_usuario']!='alumno')
	{
		header("Location:../logout.php");
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Admin</title>


</head>
<body>

<div class="wrap">
	<div id="header">
		<div id="top">
			<div class="left">
				<p>Welcome, <strong><?php echo $_SESSION['id']?> </strong> [ <a href="../logout.php">logout</a> ]</p>
			</div>
		</div>

	</div>
	
	<h1>Estamos en el panel del alumno, esta es su vista.</h1>


</div>

</body>
</html>
