<?php
        session_start();

        if(isset($_GET['status'])){
            
            $status=$_GET['status'];

            if(isset($_GET['user'])){

                $user=$_GET['user'];
                $pass=$_GET['pass'];
                $nombre=$_GET['name'];
                $ap=$_GET['ap'];
                $am=$_GET['am'];
                $td=$_GET['td'];
                $doc=$_GET['doc'];
                $fn=$_GET['fn'];
                $email=$_GET['email'];
                $rol=$_GET['rol'];
                
            }
            else{
                $user="";
                $pass="";
                $nombre="";
                $ap="";
                $am="";
                $td="";
                $doc="";
                $fn="";
                $email="";
                $rol="";
            }
        }
        else{
            $status=-1;
            if(!isset($_GET['status1'])){
                $dni=$_SESSION['documento'];
                header('Location:../../funciones/miPerfil.php?dni='.$dni);
            }
        }
         if(isset($_GET['status1'])){
            $status1=$_GET['status1'];
         }
         else{
            $status1=-1;
         }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<title>Admin</title>

</head>

<script type="text/javascript">
    function enableEditar(){
        document.getElementById('usuario1').disabled=false;
        document.getElementById('password1').disabled=false;
        document.getElementById('name1').disabled=false;
        document.getElementById('ap1').disabled=false;
        document.getElementById('am1').disabled=false;
        document.getElementById('fechaNacimiento1').disabled=false;
        document.getElementById('email1').disabled=false;
        document.getElementById('rol1').disabled=false;
        document.getElementById('guardar').style.display = 'block';
        document.getElementById('editar').style.display = 'none';
    }
</script>

<body>
    <?php 
        include_once "navigator.php";
    ?>
    
        <div id="page-content-wrapper" style="width:75%;float:right;margin-top:5%;">
            <div class="container-fluid">
                <div class="row">
                    <div class="main col-lg-12">
                        
                        <div class="alert alert-success" style=<?php if($status1==-1 or $status1!=1){ echo "display:none;";}elseif($status1==1){ echo "display:block;";}?>  >
                          <strong>Exito!</strong> Cambios guardados.
                        </div>
                        <div class="alert alert-danger" style=<?php if($status1==-1 or $status1!=0){ echo "display:none;";}elseif($status1==0){ echo "display:block;";}?> >
                          <strong>Error!</strong> Cambios no guardados.
                        </div>
                        

                        <h1 class="page-header">
                            <small>Mi perfil</small>
                        </h1>
                        <hr class="fx-line">

                        <div class="panel" style="display:block;" >
                            <form method="post" role="form" action="../../funciones/editarPerfil.php" class="login" >
                                <div class="form-group" >
                                    <label for="usuario" class="col-lg-2 control-label">Usuario:</label>
                                    <input type="text" class="form-control" name="usuario1" id="usuario1" value=<?php echo $user; ?> placeholder="Usuario" style="width:330px;" required disabled>
                                </div>

                                <div class="form-group">
                                    <label for="password" class="col-lg-2 control-label">Constraseña:</label>
                                    <input type="password" class="form-control" name="password1" id="password1" value=<?php echo $pass; ?> placeholder="Contraseña" style="width:330px;" required disabled>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-lg-2 control-label">Nombres:</label>
                                    <input type="text" class="form-control" name="name1" id="name1" value=<?php echo $nombre; ?> placeholder="Nombres" style="width:330px;" required disabled>
                                </div>
                                <div class="form-group">
                                    <label for="ap" class="col-lg-2 control-label">Apellido Paterno:</label>
                                    <input type="text" class="form-control" name="ap1" id="ap1" value=<?php echo $ap; ?> placeholder="Apellido Paterno" style="width:330px;" required disabled>
                                </div>
                                <div class="form-group">
                                    <label for="am" class="col-lg-2 control-label">Apellido Materno:</label>
                                    <input type="text" class="form-control" name="am1" id="am1" value=<?php echo $am; ?> placeholder="Apellido Materno" style="width:330px;" required disabled>
                                </div>
                                <div class="form-group">
                                    <label for="tipod" class="col-lg-2 control-label">Tipo de documento:</label>
                                    <select class="form-control" style="width:330px;" name="tipod1" id="tipod1" required readonly >
                                        <option <?php if($td=='DNI'){echo 'selected="selected"';} ?> value="DNI">DNI</option >
                                        <option <?php if($td=='PASAPORTE'){echo 'selected="selected"';} ?> value="PASAPORTE">PASAPORTE</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="documento" class="col-lg-2 control-label">Documento:</label>
                                    <input type="text" class="form-control" name="documento1" id="documento1" value=<?php echo $doc; ?> placeholder="Número de documento" style="width:330px;" required readonly >
                                </div>
                                <div class="form-group">
                                    <label for="fechaNacimiento" class="col-lg-2 control-label">Fecha de nacimiento:</label>
                                    <input type="date" class="form-control" name="fechaNacimiento1" id="fechaNacimiento1" value=<?php echo $fn; ?> style="width:330px;" required disabled>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-lg-2 control-label">Correo eléctronico:</label>
                                    <input type="email"  class="form-control" name="email1" id="email1" value=<?php echo $email; ?> style="width:330px;" required disabled>
                                </div>
                                <div class="form-group">
                                    <label for="rol" class="col-lg-2 control-label">Rol:</label>
                                    <select class="form-control" style="width:330px;"  name="rol1" id="rol1" required disabled>
                                        <option <?php if($td=='administrador'){echo 'selected="selected"';} ?> value="administrador">ADMINISTRADOR</option >
                                        <option <?php if($td=='docente'){echo 'selected="selected"';} ?> value="docente">DOCENTE</option>
                                        <option <?php if($td=='secretaria'){echo 'selected="selected"';} ?> value="secretaria">SECRETARIA</option>
                                    </select>
                                </div>
                                        <p class="login-submit">
                                            <button type="submit" class="btn btn-success" id="guardar" style="margin-left:265px;display:none;">Guardar</button>
                                        </p>
                            </form>
                            <button class="btn btn-success" onclick="enableEditar();" id="editar" style="margin-left:265px;">Editar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div>
        <div id="footer-wrapper">
            <div id="footer">
                <p class="text-muted">Desarollado por <a href="#"><em>Team gg</em></a>
                    y <a href="#"><em>CS-Unsa</em></a>. <span class="pull-right">Powered by  <a
                            href="#"><em>php7</em></a>.</span></p>
            </div>
        </div>
    </div>

</body>
</html>