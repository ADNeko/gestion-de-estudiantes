
<?php
        session_start();

        /*if(isset($_GET['status'])){
            
            $status=$_GET['status'];
            if(isset($_SESSION['carreras'])){
                $carreras=$_SESSION['carreras'];
            }
            else{
                $var=array();
            }

        }
        else{
            $status=-1;
            if(!isset($_GET['status1'])){
                header('Location:../../funciones/darCarreras.php');
            }
        }
         if(isset($_GET['status1'])){
            $status1=$_GET['status1'];
         }
         else{
            $status1=-1;
         }*/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<title>Admin</title>

</head>

<body>
    <?php 
        include_once "navigator.php";
    ?>
    
        <div id="page-content-wrapper" style="width:75%;float:right;margin-top:5%;">
            <div class="container-fluid">
                <div class="row">
                    <div class="main col-lg-12">
                        <!--
                        <div class="alert alert-success" style=<?php if($status==-1 or $status!=1){ echo "display:none;";}elseif($status==1){ echo "display:block;";}?>  >
                          <strong>Exito!</strong> Usuario encontrado.
                        </div>
                        <div class="alert alert-danger" style=<?php if($status==-1 or $status!=0){ echo "display:none;";}elseif($status==0){ echo "display:block;";}?> >
                          <strong>Error!</strong> Usuario no encontrado.
                        </div>
                        <div class="alert alert-success" style=<?php if($status1==-1 or $status1!=1){ echo "display:none;";}elseif($status1==1){ echo "display:block;";}?>  >
                          <strong>Exito!</strong> Usuario editado.
                        </div>
                        <div class="alert alert-danger" style=<?php if($status1==-1 or $status1!=0){ echo "display:none;";}elseif($status1==0){ echo "display:block;";}?> >
                          <strong>Error!</strong> Usuario no editado.
                        </div>
                        -->
                        <h1 class="page-header">
                            <small>Crear Curso</small>
                        </h1>
                        <hr class="fx-line">
                        
                        <div class="panel">
                            <form method="post" role="form" action="../../funciones/crearCurso.php" class="login" >
                                
                                <div class="form-group">
                                    <input type="text" style="display:none;" name="idCarrera" value=<?php echo $_POST['idCarrera'];?> >
                                    <input type="text" style="display:none;" name="idSemestre" value=<?php echo $_POST['idSemestre'];?> >
                                    <label for="documento" class="col-lg-2 control-label">Nombre:</label>
                                    <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre del Curso" style="width:330px;" required>
                                    <label for="documento" class="col-lg-2 control-label">Codigo:</label>
                                    <input type="text" class="form-control" name="codigo" id="codigo" placeholder="Codigo" style="width:330px;" required>
                                    <label for="documento" class="col-lg-2 control-label">Creditos:</label>
                                    <input type="text" class="form-control" name="creditos" id="creditos" placeholder="Creditos" style="width:330px;" required>
                                </div>
                                        <p class="login-submit">
                                            <button type="submit" class="btn btn-success" style="margin-left:265px;">Crear</button>
                                        </p>
                            </form>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

    <div>
        <div id="footer-wrapper">
            <div id="footer">
                <p class="text-muted">Desarollado por <a href="#"><em>Team gg</em></a>
                    y <a href="#"><em>CS-Unsa</em></a>. <span class="pull-right">Powered by  <a
                            href="#"><em>php7</em></a>.</span></p>
            </div>
        </div>
    </div>

</body>
</html>