<?php
	session_start();

    if(isset($_GET['status'])){ 
        $status=$_GET['status'];
        $idC=$_GET["idC"];
        $idI=$_GET["idI"];
        $semestres=$_SESSION['semestres'];
        $ids=$_SESSION['idSemestres'];
    }
    else{
        $idC=$_POST["idC"];
        $idI=$_POST["idI"];
        header('Location:../../funciones/darSemestresM.php?idC='.$idC.'&idI='.$idI);
    }
    if(isset($_GET['status1'])){
        $status1=$_GET['status1'];
    }  
    else{
        $status1=-1;
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<title>Admin</title>

</head>

<body>
    <?php 
        include_once "navigator.php";
    ?>
    
        <div id="page-content-wrapper" style="width:75%;float:right;margin-top:5%;">
            <div class="container-fluid">
                <div class="row">
                    <div class="main col-lg-12">
<!--
                        <div class="alert alert-danger" style=<?php if($status==-1 or $status!=0){ echo "display:none;";}elseif($status==0){ echo "display:block;";}?> >
                          <strong>Error!</strong> No hay Caja abierta.
                        </div>
-->
                        <div class="alert alert-success" style=<?php if($status1==0 or $status1!=1){ echo "display:none;";}elseif($status1==1){ echo "display:block;";}?> >
                          <strong>Exito!</strong> Matricula generada.
                        </div>
                        <div class="alert alert-danger" style=<?php if($status1==0 or $status1!=0){ echo "display:none;";}elseif($status1==0){ echo "display:block;";}?> >
                          <strong>Error!</strong> Matricula no generada.
                        </div>

                        <h1 class="page-header">
                            <small>Pagar Matricula:</small>
                        </h1>
                        <hr class="fx-line">
                        <div class="panel" >
                            <form method="post" action="../../funciones/matricular.php" role="form" class="login" style=<?php if($status==0){ echo "display:none;";}?> >
                                <input type="text" style="display:none;" name="idC" value="<?php echo $idC;?>">
                                <input type="text" style="display:none;" name="idI" value="<?php echo $idI;?>">
                                
                                <div class="form-group">
                                    <label for="tipod" class="col-lg-2 control-label">Semestre:</label>
                                    <select class="form-control" style="width:330px;" name="semestre" id="semestre" required>
                                        <?php
                                            $cont=0;
                                            foreach ($semestres as $semestre) {
                                                echo '<option value="'.$ids[0].'">'.$semestre.'</option>';
                                                $cont=$cont+1; 
                                            }
                                        ?>
                                    </select>
                                </div>
                                
                                        <p class="login-submit">
                                            <button type="submit" class="btn btn-success" style="margin-left:265px;">Pagar</button>
                                        </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div>
        <div id="footer-wrapper">
            <div id="footer">
                <p class="text-muted">Desarollado por <a href="#"><em>Team gg</em></a>
                    y <a href="#"><em>CS-Unsa</em></a>. <span class="pull-right">Powered by  <a
                            href="#"><em>php7</em></a>.</span></p>
            </div>
        </div>
    </div>

</body>
</html>
