<?php
        require_once ("../../entity/inscripcion.php");
        session_start();

        $inscripciones=array();

        if(isset($_GET['status'])){
            
            $status=$_GET['status'];

            if(isset($_GET['code'])){

                $code=$_GET['code'];
                $name=$_GET['name'];
                $ap=$_GET['ap'];
                $am=$_GET['am'];
                $td=$_GET['td'];
                $doc=$_GET['doc'];
                $fn=$_GET['fn'];
                $email=$_GET['email'];
                $cel=$_GET['cel'];
                $inscripciones=$_SESSION["inscripciones"];
            }
            else{
                $code="";
                $name="";
                $ap="";
                $am="";
                $td="";
                $doc="";
                $fn="";
                $email="";
                $cel="";
            }
        }
        else{
            $status=-1;
        }
         if(isset($_GET['status1'])){
            $status1=$_GET['status1'];
         }
         else{
            $status1=-1;
         }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<title>Admin</title>

</head>

<script>
    function submitForm(action,cont)
    {
        id='form'+cont;
        document.getElementById(id).action = action;
        document.getElementById(id).submit();
    }
</script>


<body>
    <?php 
        include_once "navigator.php";
    ?>
    
        <div id="page-content-wrapper" style="width:75%;float:right;margin-top:5%;">
            <div class="container-fluid">
                <div class="row">
                    <div class="main col-lg-12">
                        <div class="alert alert-success" style=<?php if($status==-1 or $status!=1){ echo "display:none;";}elseif($status==1){ echo "display:block;";}?>  >
                          <strong>Exito!</strong> Estudiante encontrado.
                        </div>
                        <div class="alert alert-danger" style=<?php if($status==-1 or $status!=0){ echo "display:none;";}elseif($status==0){ echo "display:block;";}?> >
                          <strong>Error!</strong> Estudiante no encontrado.
                        </div>
                        <div class="alert alert-success" style=<?php if($status1==-1 or $status1!=1){ echo "display:none;";}elseif($status1==1){ echo "display:block;";}?>  >
                          <strong>Exito!</strong> Estudiante editado.
                        </div>
                        <div class="alert alert-danger" style=<?php if($status1==-1 or $status1!=0){ echo "display:none;";}elseif($status1==0){ echo "display:block;";}?> >
                          <strong>Error!</strong> Estudiante no editado.
                        </div>
                        <h1 class="page-header">
                            <small>Busqueda de estudiante:</small>
                        </h1>
                        <hr class="fx-line">

                        <div class="panel" style=<?php if($status!=-1 and $status==1){echo "display:none;";} ?> >
                            <form method="post" role="form" action="../../funciones/buscarEstudiante.php" class="login" >
                                
                                <div class="form-group">
                                    <label for="tipod" class="col-lg-2 control-label">Tipo de documento:</label>
                                    <select class="form-control" style="width:330px;" name="tipod" id="tipod" required>
                                        <option value="DNI">DNI</option >
                                        <option value="PASAPORTE">PASAPORTE</option>
                                        <option value="CODIGO">CODIGO</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="documento" class="col-lg-2 control-label">Documento de Identidad:</label>
                                    <input type="text" class="form-control" name="documento" id="documento" placeholder="Número de documento" style="width:330px;" required>
                                </div>
                                
                                        <p class="login-submit">
                                            <button type="submit" class="btn btn-success" style="margin-left:265px;">Entrar</button>
                                        </p>
                            </form>
                        </div>

                        <div class="panel" style=<?php if($status==-1 or $status==0){echo "display:none;";} elseif($status==1){echo "display:block;";} ?> >
                            <form method="post" role="form" action="../../funciones/editarEstudiante.php" class="login" >
                                <div class="form-group">
                                    <label for="code" class="col-lg-2 control-label">Codigo:</label>
                                    <input type="text" class="form-control" name="code1" id="code1" value=<?php echo $code; ?> placeholder="Codigo" style="width:330px;" required readonly>
                                </div>
                                <div class="form-group">
                                    <label for="name" class="col-lg-2 control-label">Nombres:</label>
                                    <input type="text" class="form-control" name="name1" id="name1" value=<?php echo $name; ?> placeholder="Nombres" style="width:330px;" required disabled>
                                </div>
                                <div class="form-group">
                                    <label for="ap" class="col-lg-2 control-label">Apellido Paterno:</label>
                                    <input type="text" class="form-control" name="ap1" id="ap1" value=<?php echo $ap; ?> placeholder="Apellido Paterno" style="width:330px;" required disabled>
                                </div>
                                <div class="form-group">
                                    <label for="am" class="col-lg-2 control-label">Apellido Materno:</label>
                                    <input type="text" class="form-control" name="am1" id="am1" value=<?php echo $am; ?> placeholder="Apellido Materno" style="width:330px;" required disabled>
                                </div>
                                <div class="form-group">
                                    <label for="tipod" class="col-lg-2 control-label">Tipo de documento:</label>
                                    <select class="form-control" style="width:330px;" name="tipod1" id="tipod1" value=<?php echo $td; ?> required readonly>
                                        <option value="DNI">DNI</option >
                                        <option value="PASAPORTE">PASAPORTE</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="documento" class="col-lg-2 control-label">Documento:</label>
                                    <input type="text" class="form-control" name="documento1" id="documento1" value=<?php echo $doc; ?> placeholder="Número de documento" style="width:330px;" required readonly>
                                </div>
                                <div class="form-group">
                                    <label for="fechaNacimiento" class="col-lg-2 control-label">Fecha de nacimiento:</label>
                                    <input type="date" class="form-control" name="fechaNacimiento1" id="fechaNacimiento1" value=<?php echo $fn; ?> style="width:330px;" required disabled>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-lg-2 control-label">Correo eléctronico:</label>
                                    <input type="email"  class="form-control" name="email1" id="email1" value=<?php echo $email; ?> placeholder="Email" style="width:330px;" required disabled>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-lg-2 control-label">Celular:</label>
                                    <input type="tel"  class="form-control" name="phone1" id="phone1" value=<?php echo $cel; ?> placeholder="Celular" style="width:330px;" required disabled>
                                </div>
                                        <p class="login-submit">
                                            <button type="submit" class="btn btn-success" id="guardar" style="margin-left:265px;display:none;">Guardar</button>
                                        </p>
                            </form>
                            <button class="btn btn-success" onclick="enableEditar();" id="editar" style="margin-left:265px;">Editar</button>

                            <h1 class="page-header">
                                <small>Carreras:</small>
                            </h1>
                            <?php
                                $cont=0;
                                foreach ($inscripciones as $inscripcion) {
                                    echo '<form id="form'.$cont.'" method="post">';
                                        echo '<div class="panel panel-primary" style="margin-top:20px;padding-bottom:20px;">';
                                            echo '<div class="panel-heading">'.$inscripcion->getCarrera()->getNombre().'</div>';
                                            echo '<div class="panel-body"><strong>Estado: </strong>'.$inscripcion->getEstado().'</div>';
                                            echo '<input type="text" style="display:none;" name="idC" value="'.$inscripcion->getCarrera()->getId().'">';
                                            echo '<input type="text" style="display:none;" name="idI" value="'.$inscripcion->getId().'">';
                                            echo '<button style="margin-left:400px;margin-top:10px;" type="button" onclick=submitForm(\'../../funciones/verificarCaja.php\','.$cont.'); class="btn btn-success">Pagar Matricula</button>';
                                            echo '<button style="margin-left:40px;margin-top:10px;" type="button" onclick=submitForm(\'matricular.php\','.$cont.'); class="btn btn-success">Matricular</button>';
                                        echo '</div>' ;
                                    echo '</form>';
                                    $cont=$cont+1;
                                }
                            
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div style="position:absolute;bottom:0;">
        <div id="footer-wrapper">
            <div id="footer">
                <p class="text-muted">Desarollado por <a href="#"><em>Team gg</em></a>
                    y <a href="#"><em>CS-Unsa</em></a>. <span class="pull-right">Powered by  <a
                            href="#"><em>php7</em></a>.</span></p>
            </div>
        </div>
    </div>

</body>
</html>