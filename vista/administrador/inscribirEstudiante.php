
<?php
        require_once("../../entity/carrera.php");
        session_start();
        if(isset($_GET['status'])){
            $status=$_GET['status'];
            $carreras=$_SESSION['carreras'];
        }
        else{
            header('Location: ../../funciones/darCarreras.php');
            $status=-1;
        }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<title>Admin</title>

</head>
<body>
    <?php 
        include_once "navigator.php";
    ?>
    
        <div id="page-content-wrapper" style="width:75%;float:right;margin-top:5%;">
            <div class="container-fluid">
                <div class="row">
                    <div class="main col-lg-12">
                        <div class="alert alert-success" id="success" style=<?php if($status==-1 or $status!=1){ echo "display:none;";}elseif($status==1){ echo "display:block;";}?>  >
                          <strong>Exito!</strong> estudiante registrado.
                        </div>
                        <div class="alert alert-warning"  id="warning" style=<?php if($status==-1 or $status!=2){ echo "display:none;";}elseif($status==2){ echo "display:block;";}?> >
                          <strong>Advertencia!</strong> No se lleno los campos correctamente.
                        </div>
                        <div class="alert alert-danger" id="error" style=<?php if($status==-1 or $status!=0){ echo "display:none;";}elseif($status==0){ echo "display:block;";}?> >
                          <strong>Error!</strong> Error al registrar estudiante.
                        </div>
                        <h1 class="page-header">
                            <small>Inscripcion de Estudiante:</small>
                        </h1>
                        <hr class="fx-line">

                        <div class="panel">
                            <form method="post" role="form" action="../../funciones/crearEstudiante.php" class="login" >
                                <div class="form-group">
                                    <label for="name" class="col-lg-2 control-label">Nombres:</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Nombres" style="width:330px;" required>
                                </div>
                                <div class="form-group">
                                    <label for="ap" class="col-lg-2 control-label">Apellido Paterno:</label>
                                    <input type="text" class="form-control" name="ap" id="ap" placeholder="Apellido Paterno" style="width:330px;" required>
                                </div>
                                <div class="form-group">
                                    <label for="am" class="col-lg-2 control-label">Apellido Materno:</label>
                                    <input type="text" class="form-control" name="am" id="am" placeholder="Apellido Materno" style="width:330px;" required>
                                </div>
                                <div class="form-group">
                                    <label for="tipod" class="col-lg-2 control-label">Tipo de documento:</label>
                                    <select class="form-control" style="width:330px;" name="tipod" id="tipod" required>
                                        <option value="DNI">DNI</option >
                                        <option value="PASAPORTE">PASAPORTE</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="documento" class="col-lg-2 control-label">Documento:</label>
                                    <input type="text" class="form-control" name="documento" id="documento" placeholder="Número de documento" style="width:330px;" required>
                                </div>
                                <div class="form-group">
                                    <label for="fechaNacimiento" class="col-lg-2 control-label">Fecha de nacimiento:</label>
                                    <input type="date" class="form-control" name="fechaNacimiento" id="fechaNacimiento" style="width:330px;" required>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-lg-2 control-label">Correo eléctronico:</label>
                                    <input type="email"  class="form-control" name="email" id="email" placeholder="Email" style="width:330px;" required>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-lg-2 control-label">Celular:</label>
                                    <input type="tel"  class="form-control" name="phone" id="phone" placeholder="Celular" style="width:330px;" required>
                                </div>
                                <div class="form-group">
                                    <label for="tipod" class="col-lg-2 control-label">Carrera:</label>
                                    <select class="form-control" style="width:330px;" name="carrera" id="carrera" required>
                                        <?php
                                            foreach ($carreras as $carrera) {
                                                echo '<option value="'.$carrera->getId().'">'.$carrera->getNombre().'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                                        <p class="login-submit">
                                            <button type="submit" class="btn btn-success" style="margin-left:265px;">Inscribir</button>
                                        </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div>
        <div id="footer-wrapper">
            <div id="footer">
                <p class="text-muted">Desarollado por <a href="#"><em>Team gg</em></a>
                    y <a href="#"><em>CS-Unsa</em></a>. <span class="pull-right">Powered by  <a
                            href="#"><em>php7</em></a>.</span></p>
            </div>
        </div>
    </div>

</body>
</html>