<?php
        session_start();

        if(isset($_GET['status'])){

            $nombre=$_GET["nombre"];
            $id=$_GET["id"];
            $status=$_GET['status'];

            if(isset($_SESSION['semestres'])){
                $semestres=$_SESSION['semestres'];
                $idSemestres=$_SESSION['idSemestres'];
                $cursos=$_SESSION['datos'];
            }
            else{
                $semestres=array();
            }

        }
        else{
            $semestres=array();
            $nombre=$_POST["nombre"];
            $id=$_POST["id"];
            $status=-1;
            if(!isset($_GET['status1'])){
                header('Location:../../funciones/verCarrera.php?id='.$id.'&nombre='.$nombre);
            }
        }
        /*
        if(isset($_GET['status1'])){
            $status1=$_GET['status1'];
        }
        else{
            $status1=-1;
        }
        if(isset($_GET['status2'])){
            $status2=$_GET['status2'];
        }
        else{
            $status2=-1;
        }*/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<title>Admin</title>

</head>

<script>
    function submitForm(action,cont)
    {
        id='form'+cont;
        document.getElementById(id).action = action;
        document.getElementById(id).submit();
    }
</script>

<body>
    <?php 
        include_once "navigator.php";
    ?>
    
        <div id="page-content-wrapper" style="width:75%;float:right;margin-top:5%;">
            <div class="container-fluid">
                <div class="row">
                    <div class="main col-lg-12">
                        <!--
                        <div class="alert alert-success" style=<?php if($status1==-1 or $status1!=1){ echo "display:none;";}elseif($status1==1){ echo "display:block;";}?>  >
                          <strong>Exito!</strong> Carrera editada.
                        </div>
                        <div class="alert alert-danger" style=<?php if($status1==-1 or $status1!=0){ echo "display:none;";}elseif($status1==0){ echo "display:block;";}?> >
                          <strong>Error!</strong> Carrera no editada.
                        </div>

                        <div class="alert alert-success" style=<?php if($status2==-1 or $status2!=1){ echo "display:none;";}elseif($status2==1){ echo "display:block;";}?>  >
                          <strong>Exito!</strong> Carrera creada.
                        </div>
                        <div class="alert alert-danger" style=<?php if($status2==-1 or $status2!=0){ echo "display:none;";}elseif($status2==0){ echo "display:block;";}?> >
                          <strong>Error!</strong> Carrera no creada.
                        </div>
                        -->
                        <h1 class="page-header">
                            <small><?php echo $nombre; ?></small>
                        </h1>
                        <hr class="fx-line">

                        <h1 class="page-header">
                            <small>Semestres</small>
                        </h1>
                        <?php
                            $cont=0;
                            foreach($semestres as $semestre){
                                echo '<form method="post" id="form'.$cont.'"><div class="panel panel-primary" style="padding-bottom:20px;">';
                                    echo '<div class="panel-heading">'.$semestre.'</div>';
                                    echo '<input type="text" style="display:none;" name="nomSemestre" value="'.$semestre.'">';
                                    echo '<input type="text" style="display:none;" name="idCarrera" value="'.$id.'">'; 
                                    echo '<input type="text" style="display:none;" name="idSemestre" value="'.$idSemestres[$cont].'">';
                                foreach ($cursos[$cont] as $curso) {
                                    echo '<div class="panel-body">'.$curso.'</div>';
                                }
                                echo '<button style="margin-left:20px;margin-top:10px;" type="button" onclick="submitForm(\'crearCurso.php\','.$cont.');" class="btn btn-success">Agregar Curso</button>';
                                echo '<button style="margin-left:20px;margin-top:10px;" type="button" onclick="submitForm(\'editarSemestre.php\','.$cont.');" class="btn btn-primary">Editar Semestre</button>';
                                echo '<button style="margin-left:20px;margin-top:10px;" type="button" onclick="submitForm(\'../../funciones/eliminarSemestre.php\','.$cont.');" class="btn btn-danger">Eliminar Semestre</button>';
                                echo '</div></form>';
                                $cont=$cont+1;
                            }
                        ?>
                        <form action="crearSemestre.php" method="post">
                            <input name="id" style="display:none" value=<?php echo $id;?> >
                            <button type="submit" class="btn btn-success">Agregar Nuevo Semestre</button>
                        </form>

                        <form action="../../funciones/eliminarCarrera.php" method="post">
                             <input name="id" style="display:none" value=<?php echo $id;?> >
                            <button type="submit" class="btn btn-danger">Eliminar Carrera</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <div>
        <div id="footer-wrapper">
            <div id="footer">
                <p class="text-muted">Desarollado por <a href="#"><em>Team gg</em></a>
                    y <a href="#"><em>CS-Unsa</em></a>. <span class="pull-right">Powered by  <a
                            href="#"><em>php7</em></a>.</span></p>
            </div>
        </div>
    </div>

</body>
</html>
