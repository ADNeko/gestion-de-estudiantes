<?php
        session_start();

        if(isset($_GET['status'])){

            $estado=$_GET["estado"];
            $totalingresos=$_GET["total"];
            $total=$totalingresos;
            $status=$_GET['status'];
        }

        else{
            if(!isset($_GET['status1'])){header('Location:../../funciones/darCaja.php');}
            $status=-1;
        }
        
        if(isset($_GET['status1'])){
            $estado=$_GET["estado"];
            $totalingresos=$_GET["total"];
            $total=$totalingresos;
            $status1=$_GET['status1'];
        }
        else{
            $status1=-1;
        }/*
        if(isset($_GET['status2'])){
            $status2=$_GET['status2'];
        }
        else{
            $status2=-1;
        }*/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<title>Admin</title>

</head>

<script>
/*
    function submitForm(action,cont)
    {
        id='form'+cont;
        document.getElementById(id).action = action;
        document.getElementById(id).submit();
    }
    */
</script>

<body>
    <?php 
        include_once "navigator.php";
    ?>
    
        <div id="page-content-wrapper" style="width:75%;float:right;margin-top:5%;">
            <div class="container-fluid">
                <div class="row">
                    <div class="main col-lg-12">
                        
                        <div class="alert alert-success" style=<?php if($status==-1 or $status!=1){ echo "display:none;";}elseif($status==1){ echo "display:block;";}?>  >
                          <strong>Exito!</strong> Caja creada.
                        </div>
                        <div class="alert alert-danger" style=<?php if($status==-1 or $status!=0){ echo "display:none;";}elseif($status==0){ echo "display:block;";}?> >
                          <strong>Error!</strong> Ya existe una caja abierta.
                        </div>
                        
                        <div class="alert alert-success" style=<?php if($status1==-1 or $status1!=1){ echo "display:none;";}elseif($status1==1){ echo "display:block;";}?>  >
                          <strong>Exito!</strong> Caja cerrada.
                        </div>
                        <div class="alert alert-success" style=<?php if($status1==-1 or $status1!=0){ echo "display:none;";}elseif($status1==0){ echo "display:block;";}?> >
                          <strong>Error!</strong> Caja activada.
                        </div>
                        
                        <div class="alert alert-danger" style=<?php if($status==-1 or $status!=-2){ echo "display:none;";}elseif($status==-2){ echo "display:block;";}?> >
                          <strong>Error!</strong> No existe Caja abierta.
                        </div>
                        
                        <h1 class="page-header" style="<?php if($status==-2){echo "display:none;";}?>">
                            <small>Cajero: <?php echo $_SESSION['usuario'];?></small>
                        </h1>
                        <hr class="fx-line">
                        
                        <div class="panel panel-primary" style="<?php if($status==-2){echo "display:none;";}?>">
                          <div class="panel-heading">Datos de la Caja</div>
                          <div class="panel-body"><strong>Estado:</strong> <?php echo $estado;?></div>
                        </div>
                        
                        <div class="panel panel-primary" style="padding-bottom:20px;<?php if($status==-2){echo "display:none;";}?>">
                          <div class="panel-heading">Totales en Caja:</div>
                          <div class="panel-body"><strong>Total de ingresos:</strong> <?php echo $totalingresos;?></div>
                          <div class="panel-body"><strong>Total en Caja:</strong> <?php echo $total;?></div>
                          <form action="../../funciones/eliminarCaja.php" method="post" style="margin-left:80%;<?php if($status1==1){echo "display:none";}?>" >
                            <button type="submit" class="btn btn-danger">Cerrar Caja</button>
                          </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    <div>
        <div id="footer-wrapper">
            <div id="footer">
                <p class="text-muted">Desarollado por <a href="#"><em>Team gg</em></a>
                    y <a href="#"><em>CS-Unsa</em></a>. <span class="pull-right">Powered by  <a
                            href="#"><em>php7</em></a>.</span></p>
            </div>
        </div>
    </div>

</body>
</html>
