
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<title>Admin</title>

</head>
<body>
	<!--                HEADER              -->
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
	    <div class="container-fluid">
	    	<div class="navbar-header">        
				<a class="navbar-brand" href="index.php">SISTEMA DE MATRICULAS</a>
			</div>
			
			<div>
                <ul class="nav navbar-nav">
                    <li><a href="#"><span class="label label-success">Modo desarrollo</span></a></li>
                </ul>
            </div>

			<div class="collapse navbar-collapse" id="fx-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#"><b> <?php echo $_SESSION['usuario']; ?></b></a></li>
					<li><a title="Salir" href="../logout.php"><span class="glyphicon glyphicon-log-out"></span></a></li>
				</ul>
				
				<form class="navbar-form navbar-right" role="search">
					<input id="fx-buscador-estudiantes" type="text" class="form-control typeahead" placeholder="Buscar estudiante..." style="width: 360px">
				</form>
			</div>
		</div>
	</div>

	<!--                      NAVEGADOR                            -->
	<div id="wrapper" style="width:20%;margin-top:60px;display:inline-block;">
	    <div id="sidebar-wrapper" class="sidebar">
	        <ul class="nav nav-sidebar" style="height:630px;">
		            
	            <li class="first"><a href=""><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Inicio</a></li>
	            <li><a href="miPerfil.php"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Mi perfil</a></li>
					
				<li class="submenu-header">
					<a href="#" data-toggle="collapse" data-target="#caja-submenu" class="collapsed"><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> Caja</a>
						<ul style="list-style-type: none;" id="caja-submenu" class="submenu collapse menu_level_1">
	            			<li class="first"><a href="../../funciones/crearCaja.php"><span class="glyphicon glyphicon-chevron-right"></span> Nueva caja</a></li>
							<li class="last"><a href="miCaja.php"><span class="glyphicon glyphicon-chevron-right"></span> Mi caja</a></li>
						</ul>
	    		</li>
					
				<li class="submenu-header">
					<a href="#" data-toggle="collapse" data-target="#estudiantes-submenu" class="collapsed"><span class="glyphicon glyphicon-education" aria-hidden="true"></span> Estudiantes</a>
						<ul style="list-style-type: none;" id="estudiantes-submenu" class="submenu collapse menu_level_1">
	                		<li class="first"><a href="inscribirEstudiante.php"><span class="glyphicon glyphicon-chevron-right"></span> Inscribir</a></li>
		                	<li><a href="buscarEstudiante.php"><span class="glyphicon glyphicon-chevron-right"></span> Buscar</a></li>
							<li><a href=""><span class="glyphicon glyphicon-chevron-right"></span> Matrículas</a></li>
						</ul>
	    		</li>

				<li class="submenu-header active">
					<a href="#" data-toggle="collapse" data-target="#usuarios-submenu" class="collapsed"><span class="glyphicon glyphicon-list" aria-hidden="true"></span>	Usuarios</a>
						<ul style="list-style-type: none;" id="usuarios-submenu" class="submenu collapse menu_level_1">
		            		<li class="first"><a href="nuevoUsuario.php"><span class="glyphicon glyphicon-chevron-right"></span> Registrar</a></li>
							<li class="active last"><a href="buscarUsuario.php"><span class="glyphicon glyphicon-chevron-right"></span> Buscar</a></li>
						</ul>
				</li>

				<li class="submenu-header">
					<a href="#" data-toggle="collapse" data-target="#carrera-submenu" class="collapsed"><span class="glyphicon glyphicon-th" aria-hidden="true"></span> Configuracion</a>
						<ul style="list-style-type: none;" id="carrera-submenu" class="submenu collapse menu_level_1">
		            		<li class="first"><a href="carreras.php"><span class="glyphicon glyphicon-chevron-right"></span>Carreras</a></li>
							<li class="active last"><a href=""><span class="glyphicon glyphicon-chevron-right"></span>Buscar</a></li>
						</ul>
				</li>
			</ul>
	    </div>
	</div>

</body>
</html>
