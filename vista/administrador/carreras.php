<?php
        require_once("../../entity/carrera.php");
        session_start();

        if(isset($_GET['status'])){
            
            $status=$_GET['status'];
            if(isset($_SESSION['carreras'])){
                $carreras=$_SESSION['carreras'];
            }
            else{
                $var=array();
            }

        }
        else{
            $status=-1;
            if(!isset($_GET['status1'])){
                header('Location:../../funciones/carrera.php');
            }
        }
        if(isset($_GET['status1'])){
            $status1=$_GET['status1'];
        }
        else{
            $status1=-1;
        }
        if(isset($_GET['status2'])){
            $status2=$_GET['status2'];
        }
        else{
            $status2=-1;
        }
        if(isset($_GET['status3'])){
            $status3=$_GET['status3'];
        }
        else{
            $status3=-1;
        }
        if(isset($_GET['status4'])){
            $status4=$_GET['status4'];
        }
        else{
            $status4=-1;
        }
        if(isset($_GET['status5'])){
            $status5=$_GET['status5'];
        }
        else{
            $status5=-1;
        }
        if(isset($_GET['status6'])){
            $status6=$_GET['status6'];
        }
        else{
            $status6=-1;
        }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<title>Admin</title>

</head>

<script>
    function submitForm(action,cont)
    {
        id='carreraForm'+cont;
        document.getElementById(id).action = action;
        document.getElementById(id).submit();
    }
</script>

<body>
    <?php 
        include_once "navigator.php";
    ?>
    
        <div id="page-content-wrapper" style="width:75%;float:right;margin-top:5%;">
            <div class="container-fluid">
                <div class="row">
                    <div class="main col-lg-12">

                        <div class="alert alert-success" style=<?php if($status1==-1 or $status1!=1){ echo "display:none;";}elseif($status1==1){ echo "display:block;";}?>  >
                          <strong>Exito!</strong> Carrera editada.
                        </div>
                        <div class="alert alert-danger" style=<?php if($status1==-1 or $status1!=0){ echo "display:none;";}elseif($status1==0){ echo "display:block;";}?> >
                          <strong>Error!</strong> Carrera no editada.
                        </div>

                        <div class="alert alert-success" style=<?php if($status2==-1 or $status2!=1){ echo "display:none;";}elseif($status2==1){ echo "display:block;";}?>  >
                          <strong>Exito!</strong> Carrera creada.
                        </div>
                        <div class="alert alert-danger" style=<?php if($status2==-1 or $status2!=0){ echo "display:none;";}elseif($status2==0){ echo "display:block;";}?> >
                          <strong>Error!</strong> Carrera no creada.
                        </div>

                        <div class="alert alert-success" style=<?php if($status3==-1 or $status3!=1){ echo "display:none;";}elseif($status3==1){ echo "display:block;";}?>  >
                          <strong>Exito!</strong> Carrera eliminada.
                        </div>
                        <div class="alert alert-danger" style=<?php if($status3==-1 or $status3!=0){ echo "display:none;";}elseif($status3==0){ echo "display:block;";}?> >
                          <strong>Error!</strong> Carrera no eliminada.
                        </div>

                        <div class="alert alert-success" style=<?php if($status4==-1 or $status4!=1){ echo "display:none;";}elseif($status4==1){ echo "display:block;";}?>  >
                          <strong>Exito!</strong> Curso  creado.
                        </div>
                        <div class="alert alert-danger" style=<?php if($status4==-1 or $status4!=0){ echo "display:none;";}elseif($status4==0){ echo "display:block;";}?> >
                          <strong>Error!</strong> Curso no creado.
                        </div>

                        <div class="alert alert-success" style=<?php if($status5==-1 or $status5!=1){ echo "display:none;";}elseif($status5==1){ echo "display:block;";}?>  >
                          <strong>Exito!</strong> Semestre eliminado.
                        </div>
                        <div class="alert alert-danger" style=<?php if($status5==-1 or $status5!=0){ echo "display:none;";}elseif($status5==0){ echo "display:block;";}?> >
                          <strong>Error!</strong> Semestre no eliminado.
                        </div>

                        <div class="alert alert-success" style=<?php if($status6==-1 or $status6!=1){ echo "display:none;";}elseif($status6==1){ echo "display:block;";}?>  >
                          <strong>Exito!</strong> Semestre editado.
                        </div>
                        <div class="alert alert-danger" style=<?php if($status6==-1 or $status6!=0){ echo "display:none;";}elseif($status6==0){ echo "display:block;";}?> >
                          <strong>Error!</strong> Semestre no editado.
                        </div>

                        <h1 class="page-header">
                            <small>Carreras</small>
                        </h1>
                        <hr class="fx-line">

                        <table class="table table-condensed" style="width:80%;">
                            <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Nombre</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                /** @var carrera $carrera */
                                $cont=0;
                                foreach($carreras as $carrera){
                                        echo '<form id="carreraForm'.$cont.'" method="post">';
                                        echo '<tr>';
                                            echo '<input name="id" style="display:none" value='.$carrera->getId().'>';
                                            echo '<input name="codigo" style="display:none" value='.$carrera->getCodigo().'>';
                                            echo '<input name="nombre" style="display:none" value=\''.$carrera->getNombre().'\'>';
                                            echo '<td id="codigo">'.$carrera->getCodigo().'</td>';
                                            echo '<td id="nombre">'.$carrera->getNombre().'</td>';
                                            echo '<td>
                                                   <button type="button" class="btn btn-info" onclick="submitForm(\'verCarrera.php\','.$cont.');">Ver</button>
                                                   <button type="button" class="btn btn-warning" onclick="submitForm(\'editarCarrera.php\','.$cont.');">Editar</button></td>';
                                        echo '</tr>';
                                        echo '</form>';
                                        $cont=$cont+1;
                                    }
                                ?>
                            </tbody>
                        </table>
                        <form method="post" action="addCarrera.php">
                            <button type="submit" class="btn btn-success">Agregar nueva carrera</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <div>
        <div id="footer-wrapper">
            <div id="footer">
                <p class="text-muted">Desarollado por <a href="#"><em>Team gg</em></a>
                    y <a href="#"><em>CS-Unsa</em></a>. <span class="pull-right">Powered by  <a
                            href="#"><em>php7</em></a>.</span></p>
            </div>
        </div>
    </div>

</body>
</html>
