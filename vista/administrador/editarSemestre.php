<?php
	session_start();

    $id=$_POST['idSemestre'];
    $idC=$_POST['idCarrera'];
	$nombre=$_POST['nomSemestre'];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<title>Admin</title>

</head>

<body>
    <?php 
        include_once "navigator.php";
    ?>
    
        <div id="page-content-wrapper" style="width:75%;float:right;margin-top:5%;">
            <div class="container-fluid">
                <div class="row">
                    <div class="main col-lg-12">

                        <h1 class="page-header">
                            <small>Carreras</small>
                        </h1>
                        <hr class="fx-line">
                        <div class="panel" >
                            <form method="post" role="form" action="../../funciones/editarSemestre.php" class="login" >
                                
                                <div class="form-group">
                                    <input type="text" name="id" id="id" style="display:none;" value='<?php echo $id;?>' > 
                                    <input type="text" name="idC" id="idC" style="display:none;" value='<?php echo $idC;?>' > 
                                    <label for="documento" class="col-lg-2 control-label">Nombre:</label>
                                    <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre" value='<?php echo $nombre; ?>' style="width:330px;" required>
                                </div>
                                
                                        <p class="login-submit">
                                            <button type="submit" class="btn btn-success" style="margin-left:265px;">Editar</button>
                                        </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div>
        <div id="footer-wrapper">
            <div id="footer">
                <p class="text-muted">Desarollado por <a href="#"><em>Team gg</em></a>
                    y <a href="#"><em>CS-Unsa</em></a>. <span class="pull-right">Powered by  <a
                            href="#"><em>php7</em></a>.</span></p>
            </div>
        </div>
    </div>

</body>
</html>
